import random
import numpy as np
from tensorflow import keras
import scipy
#custom libraries
from MoleculeCompletion.Utils.featurizer import voxelize, convert_categorical_to_binary, augment_predefined_angles
from MoleculeCompletion.Utils.file_parser import sdf_parser

class SmallMoleculeStreamer(keras.utils.Sequence):
    '''
    '''
    def calc_center(self, input_array):
        array_center = np.mean((input_array.max(axis=0), input_array.min(axis=0)), axis = 0)
        return array_center

    def __init__(self, file_list, batch_size, n_augmentations=1, augment = False, dim=(16,16,16), voxel_size=0.6, shuffle = False, check_sizes = True, blur = False, blur_trunc = 4.0, empty_channel = False, max_one = False, one_channel = False, margin = 1):
        'Initialization'
        self.file_list = file_list # list of file paths AND labels
        self.batch_size = batch_size
        self.dim = dim
        self.voxel_size = voxel_size
        self.n_augmentations = n_augmentations
        self.augment = augment
        self.shuffle = shuffle
        self.blur = blur
        self.blur_trunc = blur_trunc
        self.empty_channel = empty_channel
        self.max_one = max_one
        self.one_channel = one_channel

        self.atom_types = ['C', 'H', 'O', 'N', 'P', 'S','Other']
        self.atom_type_key, self.atom_type_encoding = convert_categorical_to_binary(self.atom_types,self.atom_types)

        if n_augmentations >  batch_size:
            raise ValueError('n_augmentations cannot be larger than the batch size!')

        
        distance_constraint = dim[0] * voxel_size
        filtered_ligand_list = []
        for each in file_list:
            single_input_file = sdf_parser(each[0])
            coords_to_check = np.array(single_input_file)[:,0:3].astype(np.float)
            center_to_check = self.calc_center(coords_to_check)
            max_distance = np.linalg.norm((center_to_check - coords_to_check), axis = -1).max()
            if max_distance + margin  < distance_constraint or not check_sizes:
                filtered_ligand_list.append([each[0], center_to_check, each[1]])
        if check_sizes:
            print('Removed %d files because the ligands did not fit in the voxel grid' % (len(self.ligand_list) - len(filtered_ligand_list)))
        else:
            print('Did not check sizes of ligands against voxel grid size')
        self.ligand_list = filtered_ligand_list
        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor((len(self.ligand_list) * self.n_augmentations) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        batch_indicies = self.indexes[int(index*(self.batch_size/self.n_augmentations)):int((index+1)*(self.batch_size/self.n_augmentations))]
        # Generate data
        X, Y = self.__data_generation(batch_indicies)

        return X, Y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.ligand_list)*self.n_augmentations)
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, batch_indicies):
        'Generates data containing batch_size samples' 
        # Initialization
        n_channels = len(self.atom_type_encoding)
        if self.empty_channel:
            expanded_dim = self.dim + ((1+n_channels),)
        else:
            expanded_dim = self.dim + (n_channels,)
            

        X = np.empty((self.batch_size, *expanded_dim))
        Y = []

        for index_num, each in enumerate(batch_indicies):
            #open/read sdf file and get center
            sdf_input = sdf_parser(self.ligand_list[each][0])
            sdf_center_coords = self.ligand_list[each][1]
            label = self.ligand_list[each][2]
            sdf_input_coords = np.array(sdf_input)[:,0:3].astype(np.float)
            sdf_input_atoms = np.array(sdf_input)[:,3]
                  
            #one-hot encode atoms
            sdf_encoded_atoms = np.empty((len(sdf_input_atoms),n_channels))
            for i, atom in enumerate(sdf_input_atoms):
                if atom not in self.atom_types:
                    atom = 'Other'
                sdf_encoded_atoms[i,:] = self.atom_type_encoding[self.atom_type_key[atom]]

            if self.augment or self.n_augmentations > 1:
                sdf_rotation_vectors = np.random.randint(0, 359, (self.n_augmentations,3))
                sdf_augmented_coordinates, sdf_center_array = augment_predefined_angles(sdf_input_coords, sdf_rotation_vectors, sdf_center_coords)
                
                for i in range(self.n_augmentations):
                    aug_index = (index_num*self.n_augmentations) + i
                    #voxelize sdf
                    X[(aug_index),:] = voxelize(sdf_augmented_coordinates[i], sdf_encoded_atoms, center=sdf_center_array[i], voxel_size = self.voxel_size, grid_shape=self.dim, empty_channel = self.empty_channel)
                    Y.append(label)
            else:
                X[(index_num),:] = voxelize(sdf_input_coords, sdf_encoded_atoms, center=sdf_center_coords, voxel_size = self.voxel_size, grid_shape=self.dim, empty_channel = self.empty_channel)              
                Y.append(label)

        if self.one_channel:
            X = X.sum(axis=-1, keepdims=True)

        if self.max_one:
            X = np.array(X != 0).astype(np.int)

        if self.blur != False:
            X = X.astype(float)
            X = scipy.ndimage.gaussian_filter(X, [0,self.blur,self.blur,self.blur,0], truncate = self.blur_trunc)

        if self.blur != False and self.empty_channel:
            X[...,0] =(X[:,:,:,:,1:].sum(axis=(4)) == 0).astype(float)

        Y = np.array(Y)
        return X, Y

class SiameseSmallMoleculeStreamer(keras.utils.Sequence):
    '''
    '''
    def calc_center(self, input_array):
        array_center = np.mean((input_array.max(axis=0), input_array.min(axis=0)), axis = 0)
        return array_center

    def __init__(self, file_list, batch_size, n_augmentations=1, augment = False, dim=(16,16,16), voxel_size=0.6, shuffle = False, check_sizes = True, blur = False, blur_trunc = 4.0, empty_channel = False, max_one = False, one_channel = False, margin = 1, number_pairs = 1, pred=False):
        'Initialization'
        self.file_list = file_list # list of file paths AND labels
        self.batch_size = batch_size
        self.dim = dim
        self.voxel_size = voxel_size
        self.n_augmentations = n_augmentations
        self.augment = augment
        self.shuffle = shuffle
        self.blur = blur
        self.blur_trunc = blur_trunc
        self.empty_channel = empty_channel
        self.max_one = max_one
        self.one_channel = one_channel
        self.number_pairs = number_pairs
        self.pred = pred
        self.atom_types = ['C', 'H', 'O', 'N', 'P', 'S','Other']
        self.atom_type_key, self.atom_type_encoding = convert_categorical_to_binary(self.atom_types,self.atom_types)
        if n_augmentations > batch_size:
            raise ValueError('n_augmentations cannot be larger than the batch size!')

        
        distance_constraint = dim[0] * voxel_size
        filtered_ligand_list = []
        for each in file_list:
            single_input_file = sdf_parser(each[0])
            coords_to_check = np.array(single_input_file)[:,0:3].astype(np.float)
            center_to_check = self.calc_center(coords_to_check)
            max_distance = np.linalg.norm((center_to_check - coords_to_check), axis = -1).max()
            if max_distance + margin < distance_constraint or not check_sizes:
                filtered_ligand_list.append([each[0], center_to_check, each[1]])
        if check_sizes:
            print('Removed %d files because the ligands did not fit in the voxel grid' % (len(self.file_list) - len(filtered_ligand_list)))
        else:
            print('Did not check sizes of ligands against voxel grid size')
        self.ligand_list = filtered_ligand_list
        
        #reproducible shuffle
        random.Random(91106).shuffle(self.ligand_list)
        if self.number_pairs is None:
            self.number_pairs = len(self.ligand_list) - 1
            ranking_training_data = []
            for i, entries in enumerate(self.ligand_list):
                putative_pair_a = self.ligand_list[i]
                j = i + 1
                while j < len(self.ligand_list):
                    putative_pair_b = self.ligand_list[j]
                    comparison = putative_pair_a[2] - putative_pair_b[2]
                    ranking_training_data.append([putative_pair_a[0], putative_pair_a[1], putative_pair_b[0], putative_pair_b[1], comparison])
                    j += 1
                    
        else:
            ranking_training_data = []
            for i, entries in enumerate(self.ligand_list):
                putative_pair_a = self.ligand_list[i]
                for pair in range(int(self.number_pairs)):
                    j = (i+pair)%len(self.ligand_list)
                    putative_pair_b = self.ligand_list[j]
                    comparison = putative_pair_a[2] - putative_pair_b[2]
                    ranking_training_data.append([putative_pair_a[0], putative_pair_a[1], putative_pair_b[0], putative_pair_b[1], comparison])
        
        self.ranking_training_data = ranking_training_data
        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor((len(self.ligand_list) * self.n_augmentations) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        batch_indicies = self.indexes[int(index*(self.batch_size/self.n_augmentations)):int((index+1)*(self.batch_size/self.n_augmentations))]
        # Generate data
        X, Y = self.__data_generation(batch_indicies)
        return X, Y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.ligand_list)*self.n_augmentations)
        if self.shuffle == True:
            np.random.shuffle(self.indexes)
    def __data_generation(self, batch_indicies):
        'Generates data containing batch_size samples'

    def __data_generation(self, batch_indicies):
        'Generates data containing batch_size samples' 
        # Initialization
        n_channels = len(self.atom_type_encoding)
        if self.empty_channel:
            expanded_dim = self.dim + ((1+n_channels),)
        else:
            expanded_dim = self.dim + (n_channels,)
           
        X0 = np.empty((self.batch_size, *expanded_dim))
        X1 = np.empty((self.batch_size, *expanded_dim))
        Y = []
        prediction_data = []

        for index_num, each in enumerate(batch_indicies):
            #open/read sdf file and get center
            sdf_input_a = sdf_parser(self.ranking_training_data[each][0])
            sdf_center_coords_a = self.ranking_training_data[each][1]
            label = self.ranking_training_data[each][4]
            sdf_input_coords_a = np.array(sdf_input_a)[:,0:3].astype(np.float)
            sdf_input_atoms_a = np.array(sdf_input_a)[:,3]
            
       
            #one hot encode atoms
            sdf_encoded_atoms_a = np.empty((len(sdf_input_atoms_a),n_channels))
            for i, atom in enumerate(sdf_input_atoms_a):
                if atom not in self.atom_types:
                    atom = 'Other'
                sdf_encoded_atoms_a[i,:] = self.atom_type_encoding[self.atom_type_key[atom]]

            #second half of the pair
            sdf_input_b = sdf_parser(self.ranking_training_data[each][2])
            sdf_center_coords_b = self.ranking_training_data[each][3]
            label = self.ranking_training_data[each][4]
            sdf_input_coords_b = np.array(sdf_input_b)[:,0:3].astype(np.float)
            sdf_input_atoms_b = np.array(sdf_input_b)[:,3]

       
            #one-hot encode atoms
            sdf_encoded_atoms_b = np.empty((len(sdf_input_atoms_b),n_channels))
            for i, atom in enumerate(sdf_input_atoms_b):
                if atom not in self.atom_types:
                    atom = 'Other'
                sdf_encoded_atoms_b[i,:] = self.atom_type_encoding[self.atom_type_key[atom]]

            if self.augment or self.n_augmentations > 1:
                sdf_rotation_vectors_a = np.random.randint(0, 359, (self.n_augmentations,3))
                sdf_rotation_vectors_b = np.random.randint(0, 359, (self.n_augmentations,3))
                sdf_augmented_coordinates_a, sdf_center_array_a = augment_predefined_angles(sdf_input_coords_a, sdf_rotation_vectors_a, 
sdf_center_coords_a)
                sdf_augmented_coordinates_b, sdf_center_array_b = augment_predefined_angles(sdf_input_coords_b, sdf_rotation_vectors_b, 
sdf_center_coords_b)
                
                for i in range(self.n_augmentations):
                    aug_index = (index_num*self.n_augmentations) + i
                    #voxelize sdf
                    X0[(aug_index),:] = voxelize(sdf_augmented_coordinates_a[i], sdf_encoded_atoms_a, center=sdf_center_array_a[i], voxel_size = 
self.voxel_size, grid_shape=self.dim, empty_channel = self.empty_channel)
                    X1[(aug_index),:] = voxelize(sdf_augmented_coordinates_b[i], sdf_encoded_atoms_b, center=sdf_center_array_b[i], voxel_size = 
self.voxel_size, grid_shape=self.dim, empty_channel = self.empty_channel)
                    #get prediction data for the pair
                    prediction_data.append([self.ranking_training_data[each][0], self.ranking_training_data[each][2], 
self.ranking_training_data[each][4]])
                    Y.append(label)
            else:
                X0[(index_num),:] = voxelize(sdf_input_coords_a, sdf_encoded_atoms_a, center=sdf_center_coords_a, voxel_size = self.voxel_size, 
grid_shape=self.dim, empty_channel = self.empty_channel)
                X1[(index_num),:] = voxelize(sdf_input_coords_b, sdf_encoded_atoms_b, center=sdf_center_coords_b, voxel_size = self.voxel_size, 
grid_shape=self.dim, empty_channel = self.empty_channel)
                #get prediction data for the pair
                prediction_data.append([self.ranking_training_data[each][0], self.ranking_training_data[each][2], 
self.ranking_training_data[each][4]])
                Y.append(label)
        if self.one_channel:
            X0 = X0.sum(axis=-1, keepdims=True)
            X1 = X1.sum(axis=-1, keepdims=True)
        if self.max_one:
            X0 = np.array(X0 != 0).astype(np.int)
            X1 = np.array(X1 != 0).astype(np.int)
        if self.blur != False:
            X0 = X0.astype(float)
            X1 = X1.astype(float)
            X0 = scipy.ndimage.gaussian_filter(X0, [0,self.blur,self.blur,self.blur,0], truncate = self.blur_trunc)
            X1 = scipy.ndimage.gaussian_filter(X1, [0,self.blur,self.blur,self.blur,0], truncate = self.blur_trunc)
        if self.blur != False and self.empty_channel:
            X0[...,0] =(X0[:,:,:,:,1:].sum(axis=(4)) == 0).astype(float)
            X1[...,0] =(X1[:,:,:,:,1:].sum(axis=(4)) == 0).astype(float)

        Y = np.array(Y)
        if not self.pred:
            return [X0,X1], Y
        
        else:
            return [X0, X1], prediction_data

