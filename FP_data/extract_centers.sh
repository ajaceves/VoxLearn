#!/bin/bash
for file in all_pdbs/*.pdb
    do 
    res_name=$( grep '^MODRES' $file | awk ' { print $3 } ' | uniq )
    if [ ${#res_name} -ne 3 ]
        then
        res_name=$( awk ' $1=="HETNAM" && length($3)==3 {print $3} ' $file | uniq )

    fi
    
    if [ ${#res_name} -ne 3 ]
        then
        echo "Failed to extract Chromophore for "$file" -- Found: "$res_name
        continue
    fi
    
    chain=$( awk -v res=$res_name ' $1 ~ /HETATM/ && $4 ~ /'"$res"'/ { print $5 ; exit } ' $file )
    xyz=$( awk -v chain=$chain -v res=$res_name ' $1 ~ /HETATM/ && $4 ~ /'"$res"'/ && $5 ~/'"$chain"'/ {x+=$7; y+=$8; z+=$9; num+=1} END {print x/num","y/num","z/num} ' $file )
    name=${file##*/}
    echo ${name%%.*}','$xyz
    done

