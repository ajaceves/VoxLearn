import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import Input, Conv3D, MaxPool3D, GlobalAveragePooling3D, add, Activation, BatchNormalization, concatenate, Lambda, Flatten, Dense, Dropout
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Model
from tensorflow.keras.regularizers import l2
from tensorflow.python.keras.utils import conv_utils
from tensorflow.keras import backend as K
from kerastuner import HyperModel

from Networks.n_utils.custom_metrics import stdsq

"""
https://github.com/titu1994/Keras-DualPathNetworks/blob/master/dual_path_network.py
https://arxiv.org/pdf/1707.01629.pdf
https://towardsdatascience.com/review-dpn-dual-path-networks-image-classification-d0135dce8817

DPN won the ILSVRC 2017 Localization Challenge

Here are the filter_increment, and depth patterns of the different DPN: 
DPN92


"""
class dualpathnetwork(HyperModel):
    def __init__(self, filter_increment, depth, name):
        self.filter_increment = filter_increment
        self.depth = depth 
        self.name = name
        
        
    def build(self, hp):
       
        """
            Instantiate Dual Path Network architecture

            params
            :pdb_input: (keras tensor) input tensor with shape (32, 32, 32, 7)
            :initial_conv_filters: (int) number of filters for initial convolution bottleneck block
            :filter_increment: (list) number of filters incremented per block
                DPN-92  = [16, 32, 24, 128]
                DPN-98  = [16, 32, 32, 128]
                DPN-131 = [16, 32, 32, 128]
                DPN-107 = [20, 64, 64, 128]
            :depth: (list) 
                DPN-92  = [3, 4, 20, 3]
                DPN-98  = [3, 6, 20, 3]
                DPN-131 = [4, 8, 28, 3]
                DPN-107 = [4, 8, 20, 3]
            :cardinality: (int) cardinality factor
            :width: (int) width multiplier for network
            :base_filters: (int) number of filters for bottleneck convolution in dual_path_block
            :kernel_regularizer: (str) `l2` or `l1`
            :regularization_factor: (float) regularization factor for kernel regularizer
            :BN_momentum: (float) momentum applied to BatchNormalization

            Returns: keras compiled model instance
        """
        
        learning_rate = hp.Choice(name='learning_rate', values=[0.0001, 0.0005])
        n_filters = hp.Choice(name='n_filters',  values=[64, 80, 128])
        dropout_rate = hp.Choice(name='dropout_rate', values=[0.0, 0.3, 0.5])
        regularization_factor = hp.Choice(name='regularization_factor', values= [0.01, 0.05, 0.09])
        BN_momentum = hp.Choice(name='BatchNormalization momentum', values= [0.9, 0.99])
        
        width = hp.Choice(name='width', values=[3, 5])
        cardinality = hp.Choice(name='cardinality', values=[16, 32])
        base_filters = n_filters * 4
        initial_conv_filters = n_filters
        filter_increment = self.filter_increment
        depth = self.depth 

        inputs = Input(shape=(32, 32, 32, 7), name='pdb_read_in')

        N = inputs

        lst_depth = list(depth)
        # block 1 (initial convolution)
        N = initial_conv_block_inception(N, initial_conv_filters, l2, regularization_factor, BN_momentum)

        # block 2 (projection block)
        filter_inc = filter_increment[0]
        filters = int(cardinality * width)

        N = dual_path_block(N,
                            pointwise_filters_a=filters,
                            grouped_conv_filters_b=filters,
                            pointwise_filters_c=base_filters,
                            filter_increment=filter_inc,
                            cardinality=cardinality,
                            block_type='projection',
                            kernel_regularizer=l2,
                            regularization_factor=regularization_factor,
                            BN_momentum=BN_momentum)

        # outputs a list of two keras layers, one added, one concatenated
        for i in range(lst_depth[0] - 1):
            N = dual_path_block(N,
                                pointwise_filters_a=filters,
                                grouped_conv_filters_b=filters,
                                pointwise_filters_c=base_filters,
                                filter_increment=filter_inc,
                                cardinality=cardinality,
                                block_type='normal',
                                kernel_regularizer=l2,
                                regularization_factor=regularization_factor,
                                BN_momentum=BN_momentum)

        # remaining blocks
        for k in range(1, len(lst_depth)):
            filter_inc = filter_increment[k]
            filters *= 2
            base_filters *= 2

            N = dual_path_block(N,
                                pointwise_filters_a=filters,
                                grouped_conv_filters_b=filters,
                                pointwise_filters_c=filters,
                                filter_increment=filter_inc,
                                cardinality=cardinality,
                                block_type='downsample',
                                kernel_regularizer=l2,
                                regularization_factor=regularization_factor,
                                BN_momentum=BN_momentum)

            for i in range(lst_depth[k] - 1):
                N = dual_path_block(N,
                                    pointwise_filters_a=filters,
                                    grouped_conv_filters_b=filters,
                                    pointwise_filters_c=filters,
                                    filter_increment=filter_inc,
                                    cardinality=cardinality,
                                    block_type='normal',
                                    kernel_regularizer=l2,
                                    regularization_factor=regularization_factor,
                                    BN_momentum=BN_momentum)

        N = concatenate(N, axis=-1)

        N = GlobalAveragePooling3D()(N)
        N = Dropout(dropout_rate)(N)

        N = Flatten()(N)
        outputs = Dense(1, activation='linear', name='Output')(N)

        main_model = Model(inputs, outputs)
        adam = Adam(lr=learning_rate)

        main_model.compile(optimizer=adam, loss=['mse'], metrics=[stdsq, 'mean_squared_error'])
        main_model.summary()

        return main_model

def initial_conv_block_inception(N_input, initial_conv_filters, kernel_regularizer, regularization_factor, BN_momentum):
    '''
    adds an initial conv block, with batch normalization and reLU for the DPN
    
    params
    :N_input: (keras tensor) input tensor 
    :initial_conv_filters: (int) number of filters for initial conv block
    :kernel_regularizer: (str) `l2` or `l1`
    :regularization_factor: (float) regularization factor for kernel regularizer
    :BN_momentum: (float) momentum applied to BatchNormalization
    
    Returns: keras tensor
    '''
    N = Conv3D(filters=initial_conv_filters, 
               kernel_size=7, padding='same', 
               kernel_regularizer=kernel_regularizer(regularization_factor), 
               strides=2)(N_input)
    N = BatchNormalization(momentum=BN_momentum, axis=-1)(N)
    N = Activation('relu')(N)
    
    N = MaxPool3D(pool_size=3, strides=2, padding='same')(N)
    
    return N 

def bn_relu_conv_block(N_input, filters, kernel_regularizer, regularization_factor, BN_momentum, strides=1, kernel=3):
    """
    adds a BatchNormalization-ReLU-Conv block for DPN
    
    params
    :N_input: (keras tensor) input tensor
    :filters: number of output filters
    :kernel: (int or 3 element tuple) kernel_size
    :strides: (int or 3 element tuple) stride of a convolution
    :kernel_regularizer: (str) `l2` or `l1`
    :regularization_factor: (float) regularization factor for kernel regularizer
    :BN_momentum: (float) momentum applied to BatchNormalization
    
    Returns: keras tensor
    """
    # don't know if this piece of code does anything yet because dual_path_block already has an isinstance to take this into account
    if isinstance(N_input, list): 
        input = tuple(N_input)
        input = resdiual_path , dense_path
        N = Conv3D(filters, 
                   kernel_size=kernel, 
                   padding='same', 
                   kernel_regularizer=kernel_regularizer(regularization_factor), 
                   strides=strides)(residual_path)
        N = BatchNormalization(momentum=BN_momentum, axis=-1)(N)
        N = Activation('relu')(N)
        processed.append(N)
        
        N = Conv3D(filters, 
                   kernel_size=kernel, 
                   padding='same', 
                   kernel_regularizer=kernel_regularizer(regularization_factor), 
                   strides=strides)(dense_path)
        N = BatchNormalization(momentum=BN_momentum ,axis=-1)(N)
        N = Activation('relu')(N)
        processed.append(N)
        return processed
    else: 
        N = Conv3D(filters, kernel_size=kernel, padding='same', kernel_regularizer=kernel_regularizer(regularization_factor), strides=strides)(N_input)
        N = BatchNormalization(momentum=BN_momentum, axis=-1)(N)
        N = Activation('relu')(N)
        return N

# no kernel_initializer, l2 regularized by me
def grouped_convolution_block(N_input, grouped_channels, cardinality, strides, kernel_regularizer, regularization_factor, BN_momentum):
    """
    Adds a grouped convolution block. This block is taken from https://arxiv.org/pdf/1707.01629.pdf
    
    params
    :N_input: (keras tensor) input tensor
    :grouped_channels: (int) grouped number of filters
    :cardinality: (int) cardinality factor describing the number of groups 
    :strides: (int or 3 element tuple) performs strided convolution for downscaling if > 1
    :kernel_regularizer: (str) `l2` or `l1`
    :regularization_factor: (float) regularization factor for kernel regularizer
    :BN_momentum: (float) momentum applied to BatchNormalization
    
    Returns: keras tensor
    """
    
    group_list = []
    if cardinality == 1: 
        # with cardinality standard convolution
        N = Conv3D(grouped_channels, 
                   kernel_size=3, 
                   strides=strides, 
                   kernel_initializer='he_normal', 
                   kernel_regularizer=kernel_regularizer(regularization_factor))(N_input)
        N = BatchNormalization(momentum=BN_momentum, axis=-1)(N)
        N = Activation('relu')(N)
        return N
    
    for c in range(cardinality): 
        N = Lambda(lambda x: x[:, :, :, :, c * grouped_channels:(c+1) * grouped_channels])(N_input)
        N = Conv3D(grouped_channels, 
                   kernel_size=3, 
                   padding='same', 
                   strides=strides, 
                   kernel_regularizer=kernel_regularizer(regularization_factor))(N)
        
        group_list.append(N)
        
    group_merge = concatenate(group_list, axis=-1)
    group_merge = BatchNormalization(momentum=BN_momentum, axis=-1)(group_merge)
    group_merge = Activation('relu')(group_merge)
    
    return group_merge

def dual_path_block(N_input, pointwise_filters_a, grouped_conv_filters_b, pointwise_filters_c, filter_increment, cardinality, block_type, kernel_regularizer, regularization_factor, BN_momentum):
    """
    Creates a Dual Path Block. The first path is a ResNeXt grouped convolution block, and second block is a DenseNet dense convolution block.
    
    params
    :N_input: (keras tensor) input tensor
    :pointwise_filters_a: (int) number of filters for bottleneck pointwise convolution
    :group_conv_filters_b: (int) number of filters for grouped convolution block
    :pointwise_filters_c: (int) number of filters for the bottleneck convolution block
    :filter_increment: (int) number of filters that will be added incrementally as layers get deeper
    :cardinality: (int) cardinality factor
    :block_type: (str) determines action performed on a block 
        - `projection` : adds a projection connection
        - `downsample` : downsamples spatial resolution
        - `normal` : adds a dual path connection
    :kernel_regularizer: (str) `l2` or `l1`
    :regularization_factor: (float) regularization factor for kernel regularizer
    :BN_momentum: (float) momentum applied to BatchNormalization
        
    Returns: (list) a list of two tensors - one path for ResNeXt and another path for Densenet
    """
    init = concatenate(N_input, axis=-1) if isinstance(N_input, list) else N_input
    
    grouped_channels = int(grouped_conv_filters_b / cardinality)
    
    if block_type == 'projection':
        stride = (1,1,1)
        projection = True
    elif block_type == 'downsample':
        stride = (2,2,2)
        projection = True
    elif block_type == 'normal':
        stride = (1,1,1)
        projection = False
    else: 
        raise ValueError('`block_type` must be one of ["projection", "downsample", "normal"]. Given %s' % block_type)
    
    
    if projection: 
        projection_path = bn_relu_conv_block(init, 
                                             filters=pointwise_filters_c + 2 * filter_increment, 
                                             kernel=1, 
                                             strides=stride, 
                                             kernel_regularizer=kernel_regularizer, 
                                             regularization_factor=regularization_factor, 
                                             BN_momentum=BN_momentum)
        in_residual_path = Lambda(lambda x: x[:, :, :, :, :pointwise_filters_c])(projection_path)
        in_dense_path = Lambda(lambda x: x[:, :, :, :, pointwise_filters_c:])(projection_path)
    
    else:
        in_residual_path = N_input[0]
        in_dense_path = N_input[1]
   
    N = bn_relu_conv_block(init, 
                           filters=pointwise_filters_a, 
                           kernel=1, 
                           strides=1,
                           kernel_regularizer=kernel_regularizer, 
                           regularization_factor=regularization_factor, 
                           BN_momentum=BN_momentum)
    N = grouped_convolution_block(N, 
                                  grouped_channels=grouped_channels, 
                                  cardinality=cardinality, 
                                  strides=stride, 
                                  kernel_regularizer=kernel_regularizer, 
                                  regularization_factor=regularization_factor, 
                                  BN_momentum=BN_momentum)
    N = bn_relu_conv_block(N, 
                           filters=pointwise_filters_c + filter_increment, 
                           kernel=1,
                           strides=1,
                           kernel_regularizer=kernel_regularizer, 
                           regularization_factor=regularization_factor, 
                           BN_momentum=BN_momentum)
    
    out_residual_path = Lambda(lambda x: x[:, :, :, :, :pointwise_filters_c])(N)
    out_dense_path = Lambda(lambda x: x[:, :, :, :, pointwise_filters_c:])(N)
    
    residual_path = add([in_residual_path, out_residual_path])
    dense_path = concatenate([in_dense_path, out_dense_path], axis=-1)
    
    return [residual_path, dense_path]
