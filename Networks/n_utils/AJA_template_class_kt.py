import numpy as np 

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv3D, MaxPool3D, BatchNormalization, Input, AveragePooling3D, Lambda, ReLU, Add, concatenate
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.regularizers import l2
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Model
from tensorflow.keras.callbacks import EarlyStopping, TensorBoard
from tensorflow.keras import backend as K
from kerastuner import HyperModel

from Networks.n_utils.custom_metrics import stdsq

class AJA_template(HyperModel):
    def __init__(self, name):
        self.name = name

    def build(self, hp):
        # no batch normalization inlcuded; made into a 3D network from Table 1. https://arxiv.org/pdf/1512.00567v2.pdf
        # n_filters start at 4, for original values.

        n_filters = hp.Choice('number of filters', values=[4, 6, 8])
        regularization_factor = hp.Choice('regularization factor', values=[0.01, 0.05, 0.09])
        dropout_rate = hp.Choice('dropout rate', values=[0.1, 0.3, 0.5])
        learning_rate = hp.Choice('learning rate', values=[0.0001, 0.0005])
        inputs = Input(shape=(32, 32, 32, 7), name='pdb_read_in')

        N = inputs
        N = Conv3D(filters=n_filters * (12),
                   kernel_size=6,
                   padding='same',
                   data_format="channels_last")(N)
        N = BatchNormalization()(N)
        N = Activation('relu')(N)
        N = MaxPool3D(padding="same", pool_size=2)(N)

        N = inception_AJA_6(N,
                            n_filters * (16),
                            n_filters * (24),
                            n_filters * (32),
                            n_filters * (4),
                            n_filters * (8),
                            n_filters * (8),
                            l2,
                            regularization_factor)
        N = MaxPool3D(padding="same", pool_size=2)(N)

        N = inception_AJA_3(N,
                            n_filters * (40),
                            n_filters * (28),
                            n_filters * (56),
                            n_filters * (6),
                            n_filters * (16),
                            n_filters * (16),
                            l2,
                            regularization_factor)
        N = inception_AJA_3(N,
                            n_filters * (96),
                            n_filters * (48),
                            n_filters * (96),
                            n_filters * (12),
                            n_filters * (32),
                            n_filters * (32),
                            l2,
                            regularization_factor)
        N = AveragePooling3D(padding="same", pool_size=7, strides=7)(N)

        # Always flatten arrays or tensors before adding Dense layers:
        N = Flatten()(N)
        N = Dense(256, activation="relu")(N)
        N = Dense(128, activation="relu")(N)
        N = Dropout(dropout_rate)(N)

        outputs = Dense(1, activation='linear', name='Output')(N)

        # here we declare the model starts at "pdb_input" and ends and "N_out". Keras will link all of the layers in between. Note we can reuse layer names (like we did with "N"), but the input and output layer names must be unique
        main_model = Model(inputs, outputs)
        adam = Adam(lr=learning_rate)

        main_model.compile(optimizer=adam, loss=['mse'], metrics=[stdsq, 'mean_squared_error'])
        main_model.summary()

        return main_model

def inception_AJA_3(N, filters_1x1x1, filters_3x3x3_reduce, filters_1x1x3, filters_1x3x1, filters_3x1x1,
                        filters_pool_proj, kernel_regularizer, regularization_factor):
        """
        inception module to reduce # of parameters in AJA_template

        params
        :N: (keras tensor) input tensor
        :filters_1x1x1: (int or 3 element tuple) number of filters for convolution
        :filters_3x3x3_reduce: (int or 3 element tuple) number of filters for convolution
        :filters_1x1x3: (int or 3 element tuple) number of filters for convolution
        :filters_1x3x1: (int or 3 element tuple) number of filters for convolution
        :filters_3x1x1: (int or 3 element tuple) number of filters for convolution
        :filters_pool_proj: (int or 3 element tuple) number of filters for convolution
        :kernel_regularizer: (string) `l2` or `l1` expected
        :regularization_factor: (int) regularization factor for regularization penalty

        Returns: keras tensor
        """
        conv_1x1x1 = Conv3D(filters=filters_1x1x1,
                            kernel_size=1, padding='same',
                            activation='relu',
                            kernel_regularizer=kernel_regularizer(regularization_factor))(N)

        conv_3x3x3_reduce = Conv3D(filters=filters_3x3x3_reduce,
                                   kernel_size=1,
                                   padding='same',
                                   activation='relu',
                                   kernel_regularizer=kernel_regularizer(regularization_factor))(N)
        conv_1x1x3 = Conv3D(filters=filters_1x1x3,
                            kernel_size=(1, 1, 3),
                            padding='same',
                            activation='relu',
                            kernel_regularizer=kernel_regularizer(regularization_factor))(conv_3x3x3_reduce)
        conv_1x3x1 = Conv3D(filters=filters_1x3x1,
                            kernel_size=(1, 3, 1),
                            padding='same',
                            activation='relu',
                            kernel_regularizer=kernel_regularizer(regularization_factor))(conv_1x1x3)
        conv_3x1x1 = Conv3D(filters=filters_3x1x1,
                            kernel_size=(3, 1, 1),
                            padding='same',
                            activation='relu',
                            kernel_regularizer=kernel_regularizer(regularization_factor))(conv_1x3x1)

        maxpool = MaxPool3D(pool_size=3,
                            strides=1,
                            padding='same')(N)
        maxpool_proj = Conv3D(filters=filters_pool_proj,
                              kernel_size=1,
                              padding='same',
                              activation='relu',
                              kernel_regularizer=kernel_regularizer(regularization_factor))(maxpool)

        inception_output = concatenate([conv_1x1x1, conv_1x1x3, conv_1x3x1, conv_3x1x1, maxpool_proj], axis=-1)

        return inception_output

def inception_AJA_6(N, filters_1x1x1, filters_6x6x6_reduce, filters_1x1x6, filters_1x6x1, filters_6x1x1,
                        filters_pool_proj, kernel_regularizer, regularization_factor):
        """
        inception module to reduce # of parameters in AJA_template

        params
        :N: (keras tensor) input tensor
        :filters_1x1x1: (int or 3 element tuple) number of filters for convolution
        :filters_6x6x6_reduce: (int or 3 element tuple) number of filters for convolution
        :filters_1x1x6: (int or 3 element tuple) number of filters for convolution
        :filters_1x6x1: (int or 3 element tuple) number of filters for convolution
        :filters_6x1x1: (int or 3 element tuple) number of filters for convolution
        :filters_pool_proj: (int or 3 element tuple) number of filters for convolution
        :kernel_regularizer: (string) `l2` or `l1` expected
        :regularization_factor: (int) regularization factor for regularization penalty

        Returns: keras tensor
        """
        conv_1x1x1 = Conv3D(filters=filters_1x1x1,
                            kernel_size=1,
                            padding='same',
                            activation='relu',
                            kernel_regularizer=kernel_regularizer(regularization_factor))(N)

        conv_6x6x6_reduce = Conv3D(filters=filters_6x6x6_reduce,
                                   kernel_size=1,
                                   padding='same',
                                   activation='relu',
                                   kernel_regularizer=kernel_regularizer(regularization_factor))(N)
        conv_1x1x6 = Conv3D(filters=filters_1x1x6,
                            kernel_size=(1, 1, 6),
                            padding='same',
                            activation='relu',
                            kernel_regularizer=kernel_regularizer(regularization_factor))(conv_6x6x6_reduce)
        conv_1x6x1 = Conv3D(filters=filters_1x6x1,
                            kernel_size=(1, 6, 1),
                            padding='same',
                            activation='relu',
                            kernel_regularizer=kernel_regularizer(regularization_factor))(conv_1x1x6)
        conv_6x1x1 = Conv3D(filters=filters_6x1x1,
                            kernel_size=(6, 1, 1),
                            padding='same',
                            activation='relu',
                            kernel_regularizer=kernel_regularizer(regularization_factor))(conv_1x6x1)

        maxpool = MaxPool3D(pool_size=3, strides=1, padding='same')(N)
        maxpool_proj = Conv3D(filters=filters_pool_proj, kernel_size=1, padding='same', activation='relu',
                              kernel_regularizer=kernel_regularizer(regularization_factor))(maxpool)

        inception_output = concatenate([conv_1x1x1, conv_1x1x6, conv_1x6x1, conv_6x1x1, maxpool_proj], axis=-1)

        return inception_output