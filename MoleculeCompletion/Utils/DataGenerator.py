#python libraries
import copy, glob, csv, random, ast
#3rd party libraries
import numpy as np
import keras
import skimage
import scipy
from skimage import transform
#custom libraries
from MoleculeCompletion.Utils.featurizer import voxelize, augment_dataset, convert_categorical_to_binary, augment_predefined_angles
from MoleculeCompletion.Utils.file_parser import pdb_parser, sdf_parser

class DataGenerator(keras.utils.Sequence):
    '''
    '''
    def __init__(self, list_files, batch_size, n_augmentations=1, dim=(64,64,64), shuffle = False):
        'Initialization'
        self.list_files = list_files
        self.batch_size = batch_size
        self.dim = dim
        self.n_augmentations = n_augmentations
        self.shuffle = shuffle
        self.on_epoch_end()

        #TODO: Add automatic mapping to "Other"
        atom_types = ['C', 'H', 'O', 'N', 'P', 'S']
        self.atom_type_key, self.atom_type_encoding = convert_categorical_to_binary(atom_types,atom_types)

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor((len(self.list_files) * self.n_augmentations) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        batch_indicies = self.indexes[int(index*(self.batch_size/self.n_augmentations)):int((index+1)*(self.batch_size/self.n_augmentations))]
        # Generate data
        X0, X1, Y = self.__data_generation(batch_indicies)

        return [X0,X1], Y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_files)*self.n_augmentations)
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, batch_indicies):
        'Generates data containing batch_size samples' 
        # Initialization
        n_channels = len(self.atom_type_encoding)
        expanded_dim = self.dim + (n_channels,)
        #add channels here
        X0 = np.empty((self.batch_size, *expanded_dim))
        X1 = np.empty((self.batch_size, *expanded_dim))
        Y = []

        '''Ideas:
        Currently pre-defining Ab-Ag pairs, could move that step into this generator function, making the process more random
        '''
        for index, each in enumerate(batch_indicies):
            #open/read file and get center
            each = self.list_files[each]
            Ab_file = pdb_parser(each[0])
            Ab_coords = np.array(Ab_file)[:,0:3].astype(np.float)
            Ab_atoms = np.array(Ab_file)[:,3]
            center_coords = Ab_coords.mean(axis=0)
      
            #one-hot encode atoms
            encoded_atoms = np.empty((len(Ab_atoms),n_channels))
            for i, atom in enumerate(Ab_atoms):
                encoded_atoms[i,:] = self.atom_type_encoding[self.atom_type_key[atom]]

            #augment coords
            augmented_Ab_coordinates, center_array = augment_dataset(Ab_coords, self.n_augmentations, center_coords)

            for i in range(self.n_augmentations):
                voxelized_ab = voxelize(augmented_Ab_coordinates[i], encoded_atoms, center=center_array[i], voxel_size = 1, grid_shape=self.dim)
                X0[(index+i),:] = voxelized_ab

            #now antigen
            Ag_file = pdb_parser(each[1])
            Ag_coords = np.array(Ag_file)[:,0:3].astype(np.float)
            Ag_atoms = np.array(Ag_file)[:,3]
            center_coords = Ag_coords.mean(axis=0)
      
            encoded_atoms = np.empty((len(Ag_atoms),n_channels))
            for i, atom in enumerate(Ag_atoms):
                encoded_atoms[i,:] = self.atom_type_encoding[self.atom_type_key[atom]]
           
            #augment coords
            augmented_Ag_coordinates, center_array = augment_dataset(Ag_coords, self.n_augmentations, center_coords)

            for i in range(self.n_augmentations):
                voxelized_ag = voxelize(augmented_Ag_coordinates[i], encoded_atoms, center=center_array[i], voxel_size = 1, grid_shape=self.dim)
                X1[(index+i),:] = voxelized_ag
                #label must also be appended repeatedly
                Y.append(int(each[2]))

        return X0, X1, Y

class SiameseGenerator(keras.utils.Sequence):
    def __init__(self, log_file, data_directory, batch_size, n_augmentations=1, dim=(64,64,64), vox_size =1, max_imbalance = 4, augment = False, shuffle = True, max_one = False, one_channel = False):
        #further afield... aa level definitions
        #undo size filtering, center crop on CDRs
        #If center is false, center of CDR/AG interface is left as center
        #if the length of a line in the log file is four, detect that there are two centers provided and apply them automatically
        
        'Initialization'
        self.log_file = log_file
        self.data_directory = data_directory.rstrip('/') + str('/')
        self.batch_size = batch_size
        self.dim = dim
        self.vox_size = vox_size
        self.n_augmentations = n_augmentations
        self.shuffle = shuffle
        self.max_imbalance = int(max_imbalance)
        self.augment = augment
        self.max_one = max_one
        self.one_channel = one_channel

        #TODO: Add automatic mapping to "Other"
        self.atom_types = ['C', 'H', 'O', 'N', 'P', 'S','Other']
        self.atom_type_key, self.atom_type_encoding = convert_categorical_to_binary(self.atom_types,self.atom_types)

        with open(self.log_file, 'r') as f:
            reader = csv.reader(f)
            data_list = list(reader)

        #preload all data into list like this:
        preloaded_data = []
        for each_line in data_list:
            pdb_code = each_line[0]
            if len(each_line) == 4:
                center_coords = [ast.literal_eval(each_line[1]), ast.literal_eval(each_line[2])]
            elif len(each_line) == 3:
                center_coords = [ast.literal_eval(each_line[1])]
            else:
                raise ValueError('Log format is incorrect!')     
            Ab_file = pdb_parser(self.data_directory + pdb_code + "_Ab.pdb")
            Ab_coords = np.array(Ab_file)[:,0:3].astype(np.float)
            Ab_atoms = np.array(Ab_file)[:,3]
            Ag_file = pdb_parser(self.data_directory + pdb_code + "_Ag.pdb")
            Ag_coords = np.array(Ag_file)[:,0:3].astype(np.float)
            Ag_atoms = np.array(Ag_file)[:,3]
            preloaded_data.append([pdb_code,center_coords,Ab_coords,Ab_atoms,Ag_coords,Ag_atoms])
        self.preloaded_data = preloaded_data

        list_pairs = []
        #REPLACE THIS:
        #start with all good pairs, then generate len(good) * imbalance bad pairs, by, for each good, picking a random bad(except self)
        #then shuffle
        master_length = len(preloaded_data)
        master_index_list = list(range(master_length))
        data_list= []
        for indicies in master_index_list:
            data_list.append([indicies,indicies,1])
            #append next n entries as negative pairs
            for false_num in range(1,self.max_imbalance+1):
                false_index = indicies + false_num
                if false_index >= master_length:
                    false_index = false_index%master_length
                data_list.append([indicies,false_index,0])
            
        random.shuffle(data_list)
        self.data_list = data_list
        self.on_epoch_end()

    def __len__(self):
        return int(np.floor((len(self.data_list) * self.n_augmentations) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        batch_indicies = self.indexes[int(index*(self.batch_size/self.n_augmentations)):int((index+1)*(self.batch_size/self.n_augmentations))]
        # Generate data
        X0, X1, Y = self.__data_generation(batch_indicies)

        return [X0,X1], Y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.data_list))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, batch_indicies):
        'Generates data containing batch_size samples' 
        # Initialization
        n_channels = len(self.atom_type_encoding)
        expanded_dim = self.dim + (n_channels,)
        #add channels here
        X0 = np.empty((self.batch_size, *expanded_dim))
        X1 = np.empty((self.batch_size, *expanded_dim))
        Y = []

        for index_num, single_index in enumerate(batch_indicies):
            #open/read file and get center
            subindicies_and_label = self.data_list[single_index]
            entry_label = int(subindicies_and_label[2])
            pdb_code = self.preloaded_data[subindicies_and_label[0]][0]
            Ab_coords = self.preloaded_data[subindicies_and_label[0]][2]
            Ab_atoms = self.preloaded_data[subindicies_and_label[0]][3]
            #selects first of centers, if there are two or one, working either way
            center_coords = np.array(self.preloaded_data[subindicies_and_label[0]][1][0])
            #one-hot encode atoms
            encoded_atoms = np.empty((len(Ab_atoms),n_channels))
            for i, atom in enumerate(Ab_atoms):
                if atom not in self.atom_types:
                    atom = 'Other'
                encoded_atoms[i,:] = self.atom_type_encoding[self.atom_type_key[atom]]

            #augment coords
            if self.augment or self.n_augmentations > 1:
                augmented_Ab_coordinates, center_array = augment_dataset(Ab_coords, self.n_augmentations, center_coords)
                for i in range(self.n_augmentations):
                    voxelized_ab = voxelize(augmented_Ab_coordinates[i], encoded_atoms, center=center_array[i], voxel_size = self.vox_size, grid_shape=self.dim)
                    aug_index = (index_num*self.n_augmentations) + i   
                    X0[(aug_index),:] = voxelized_ab
            #no augmenting, just voxelize and add            
            else:
                voxelized_ab = voxelize(Ab_coords, encoded_atoms, center=center_coords, voxel_size = self.vox_size, grid_shape=self.dim)
                X0[(index_num),:] = voxelized_ab

            #now antigen
            Ag_coords = self.preloaded_data[subindicies_and_label[1]][4]
            Ag_atoms = self.preloaded_data[subindicies_and_label[1]][5]
            #selects last of centers, if there are two or one, working either way
            center_coords = np.array(self.preloaded_data[subindicies_and_label[1]][1][-1])
      
            #one-hot encode atoms
            encoded_atoms = np.empty((len(Ag_atoms),n_channels))
            for i, atom in enumerate(Ag_atoms):
                if atom not in self.atom_types:
                    atom = 'Other'
                encoded_atoms[i,:] = self.atom_type_encoding[self.atom_type_key[atom]]

            #augment coords
            if self.augment or self.n_augmentations > 1:
                augmented_Ag_coordinates, center_array = augment_dataset(Ag_coords, self.n_augmentations, center_coords)

                for i in range(self.n_augmentations):
                    voxelized_ag = voxelize(augmented_Ag_coordinates[i], encoded_atoms, center=center_array[i], voxel_size = self.vox_size, grid_shape=self.dim)
                    aug_index = (index_num*self.n_augmentations) + i
                    X1[(aug_index),:] = voxelized_ag
                    Y.append(entry_label)
            #no augmenting, just voxelize and add            
            else:
                voxelized_ag = voxelize(Ag_coords, encoded_atoms, center=center_coords, voxel_size = self.vox_size, grid_shape=self.dim)
                X1[(index_num),:] = voxelized_ag
                Y.append(entry_label)
        
        if self.one_channel:
            X0 = X0.sum(axis=-1, keepdims=True)
            X1 = X1.sum(axis=-1, keepdims = True)
        if self.max_one:
            X0 = np.array(X0 != 0).astype(np.int)
            X1 = np.array(X1 != 0).astype(np.int)
        return X0, X1, Y

#things are coming out empty. Also, slow, maybe we should have all pdbs in memory?
#all pdbs total 250 MB... preparse! then how to access... as dictionary, or list of lists?
#idea one: two dictionaries, Ag and Ab
#idea two: list of lists :[pdb_code,ab, ag, center] and access by indicies <--- start here

class SiameseGeneratorDiskRead(keras.utils.Sequence):
    #TODO!!! Errors present, retrurning empty arrays. check later if we end up keeping this structure
    def __init__(self, log_file, data_directory, batch_size, n_augmentations=1, dim=(64,64,64), vox_size =1, max_imbalance = 4, augment = False, shuffle = True):
        #what about cropping?
        #undo size filtering, center crop on CDRs
        #If center is false, center of CDR/AG interface is left as center
        
        'Initialization'
        self.log_file = log_file
        self.data_directory = data_directory.rstrip('/') + str('/')
        self.batch_size = batch_size
        self.dim = dim
        self.vox_size = vox_size
        self.n_augmentations = n_augmentations
        self.shuffle = shuffle
        self.max_imbalance = max_imbalance
        self.augment = augment

        #TODO: Add automatic mapping to "Other"
        self.atom_types = ['C', 'H', 'O', 'N', 'P', 'S','Other']
        self.atom_type_key, self.atom_type_encoding = convert_categorical_to_binary(self.atom_types,self.atom_types)

        #rather than glob both now, use names selected from log to load
        with open(self.log_file, 'r') as f:
            reader = csv.reader(f)
            data_list = list(reader)

        #generate pairs of both lists:
        list_pairs = []
        sum_true = 0

        #REPLACE THIS:
        #start with all good pairs, then generate len(good) * imbalance bad pairs, by, for each good, picking a random bad(except self)
        #then shuffle
        master_length = len(data_list)
        paired_data_list= []
        for index, lines in enumerate(data_list):
                #append true pair
                #TODO: add filtering ability if extent is larger than box
                paired_data_list.append([lines[0],lines[0],lines[1],lines[1],1])
                #append next n entries as negative pairs
                for false_num in range(1,self.max_imbalance+1):
                    false_index = index + false_num
                    if false_index >= master_length:
                        false_index = false_index%master_length
                    false_line = data_list[false_index]
                    paired_data_list.append([lines[0],false_line[0],lines[1],false_line[1],0])
            
        random.shuffle(paired_data_list)
        self.paired_data_list = paired_data_list
        self.on_epoch_end()

    def __len__(self):
        return int(np.floor((len(self.paired_data_list) * self.n_augmentations) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        batch_indicies = self.indexes[int(index*(self.batch_size/self.n_augmentations)):int((index+1)*(self.batch_size/self.n_augmentations))]
        # Generate data
        X0, X1, Y = self.__data_generation(batch_indicies)

        return [X0,X1], Y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.paired_data_list))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, batch_indicies):
        'Generates data containing batch_size samples' 
        # Initialization
        n_channels = len(self.atom_type_encoding)
        expanded_dim = self.dim + (n_channels,)
        #add channels here
        X0 = np.empty((self.batch_size, *expanded_dim))
        X1 = np.empty((self.batch_size, *expanded_dim))
        Y = []

        for index_num, single_index in enumerate(batch_indicies):
            #open/read file and get center
            pair_data = self.paired_data_list[single_index]
            pdb_code = pair_data[0]
            entry_label = int(pair_data[4])
            Ab_file = pdb_parser(self.data_directory + pdb_code + "_Ab.pdb")
            Ab_coords = np.array(Ab_file)[:,0:3].astype(np.float)
            Ab_atoms = np.array(Ab_file)[:,3]
            center_coords = np.array(ast.literal_eval(pair_data[2]))
      
            #one-hot encode atoms
            encoded_atoms = np.empty((len(Ab_atoms),n_channels))
            for i, atom in enumerate(Ab_atoms):
                if atom not in self.atom_types:
                    atom = 'Other'
                    encoded_atoms[i,:] = self.atom_type_encoding[self.atom_type_key[atom]]

            #augment coords
            if self.augment or self.n_augmentations > 1:
                augmented_Ab_coordinates, center_array = augment_dataset(Ab_coords, self.n_augmentations, center_coords)

                for i in range(self.n_augmentations):
                    voxelized_ab = voxelize(augmented_Ab_coordinates[i], encoded_atoms, center=center_array[i], voxel_size = self.vox_size, grid_shape=self.dim)
                    aug_index = (index_num*self.n_augmentations) + i   
                    X0[(aug_index),:] = voxelized_ab
            #no augmenting, just voxelize and add            
            else:
                voxelized_ab = voxelize(Ab_coords, encoded_atoms, center=center_coords, voxel_size = self.vox_size, grid_shape=self.dim)
                X0[(index_num),:] = voxelized_ab

            #now antigen
            Ag_file = pdb_parser(self.data_directory + pdb_code + "_Ag.pdb")
            Ag_coords = np.array(Ag_file)[:,0:3].astype(np.float)
            Ag_atoms = np.array(Ag_file)[:,3]
            center_coords = np.array(ast.literal_eval(pair_data[3]))
      
            #one-hot encode atoms
            encoded_atoms = np.empty((len(Ag_atoms),n_channels))
            for i, atom in enumerate(Ag_atoms):
                if atom not in self.atom_types:
                    atom = 'Other'
                    encoded_atoms[i,:] = self.atom_type_encoding[self.atom_type_key[atom]]

            #augment coords
            if self.augment or self.n_augmentations > 1:
                augmented_Ag_coordinates, center_array = augment_dataset(Ag_coords, self.n_augmentations, center_coords)

                for i in range(self.n_augmentations):
                    voxelized_ag = voxelize(augmented_Ag_coordinates[i], encoded_atoms, center=center_array[i], voxel_size = self.vox_size, grid_shape=self.dim)
                    aug_index = (index_num*self.n_augmentations) + i
                    X1[(aug_index),:] = voxelized_ag
                    Y.append(entry_label)
            #no augmenting, just voxelize and add            
            else:
                voxelized_ag = voxelize(Ag_coords, encoded_atoms, center=center_coords, voxel_size = self.vox_size, grid_shape=self.dim)
                X1[(index_num),:] = voxelized_ag
                Y.append(entry_label)        
        return X0, X1, Y


class ToyGenerator(keras.utils.Sequence):
    #TODO: No jitter yet. at current resolution it makes things extra blurry
    '''
    Note, cannot apply shift if augment is False.
    '''
    def __init__(self, l_array, r_array, batch_size, n_augmentations = 1, dim=(28,28), shuffle = True, max_imbalance = 100, augment = False, shift_type = None):
        'Initialization'
        self.batch_size = batch_size
        self.dim = dim
        self.shuffle = shuffle
        self.max_imbalance = int(max_imbalance)
        self.n_augmentations = n_augmentations
        self.augment = augment
        self.l_array = l_array
        self.r_array = r_array
        self.shift_type = shift_type

        #generate pairs of both lists:
        list_pairs = []
        sum_true = 0

        #REPLACE THIS:
        #start with all good pairs, then generate len(good) * imbalance bad pairs, by, for each good, picking a random bad(except self)
        #then shuffle
        master_length = len(l_array)
        master_index_list = list(range(master_length))
        data_list= []
        for indicies in master_index_list:
                #append true pair
                data_list.append([indicies,indicies,1])
                #append next n entries as negative pairs
                for false_num in range(1,self.max_imbalance+1):
                    false_index = indicies + false_num
                    if false_index >= master_length:
                        false_index = false_index%master_length
                    data_list.append([indicies,false_index,0])
            
        random.shuffle(data_list)
        self.list_pairs = data_list
        self.on_epoch_end()

    def __len__(self):
        return int(np.floor((len(self.list_pairs) * self.n_augmentations) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        batch_indicies = self.indexes[int(index*(self.batch_size/self.n_augmentations)):int((index+1)*(self.batch_size/self.n_augmentations))]
        # Generate data
        X0, X1, Y = self.__data_generation(batch_indicies)
        if np.isnan(X0).any() or np.isnan(X1).any():
            print('BROKEN')       
            raise SystemExit(0)

        return [X0,X1], Y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_pairs))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def get_coords(self, img_array):
        coords = []
        for i in range(img_array.shape[0]):
            for j in range(img_array.shape[1]):
                if img_array[i][j]:
                    coords.append((i,j))
        return(np.array(coords))

    def shift(self, arr, x_off, y_off, fill_value=0):
        y_shifted_result = np.empty_like(arr)
        x_off = int(x_off)
        y_off = int(y_off)
        #first y_offset
        if y_off > 0:
            y_shifted_result[:y_off] = fill_value
            y_shifted_result[y_off:] = arr[:-y_off]
        elif y_off < 0:
            y_shifted_result[y_off:] = fill_value
            y_shifted_result[:y_off] = arr[-y_off:]
        else:
            y_shifted_result = arr
        
        x_shifted_result = np.empty_like(y_shifted_result)
        if x_off > 0:
            x_shifted_result[:,:x_off] = fill_value
            x_shifted_result[:,x_off:] = y_shifted_result[:,:-x_off]
        elif x_off < 0:
            x_shifted_result[:,x_off:] = fill_value
            x_shifted_result[:,:x_off] = y_shifted_result[:,-x_off:]
        return x_shifted_result

    def center_array(self, input_array, size=28):
        center = self.get_coords(input_array).mean(axis=0)
        offset = np.array([size/2,size/2]) - center
        centered_array = self.shift(input_array, offset[1], offset[0])
        return centered_array

    def __data_generation(self, batch_indicies):
        'Generates data containing batch_size samples' 
        # Initialization
        n_channels = 1
        expanded_dim = self.dim + (n_channels,)
        #add channels here
        X0 = np.empty((self.batch_size, *self.dim))
        X1 = np.empty((self.batch_size, *self.dim))
        Y = []
        if self.shift_type not in (None, 'center'):
            raise ValueError('shift_type %s is invalid' % (self.shift_type))     

        if self.augment or self.n_augmentations > 1:
            for index, each in enumerate(batch_indicies):
                l = self.l_array[self.list_pairs[each][0]]
                r = self.r_array[self.list_pairs[each][1]]
                if self.shift_type == 'center':
                    l = self.center_array(l, size=self.dim[0])
                    r = self.center_array(r, size=self.dim[0])
                rotation_degs = np.random.randint(0, 360, size=self.n_augmentations)
                for j, degrees in enumerate(list(rotation_degs)):
                  aug_index = (index*self.n_augmentations) + j   
                  X0[aug_index,:,:] = skimage.transform.rotate(l, degrees, resize=False, order=1, preserve_range=True)
                rotation_degs = np.random.randint(0, 360, size=self.n_augmentations)
                for j, degrees in enumerate(list(rotation_degs)):
                  aug_index = (index*self.n_augmentations) + j       
                  X1[aug_index,:,:] = skimage.transform.rotate(r, degrees, resize=False, order=1, preserve_range=True)
                  Y.append(int(self.list_pairs[each][2]))
        else:
            for index, each in enumerate(batch_indicies):
                X0[index,:,:] = self.l_array[self.list_pairs[each][0]]
                X1[index,:,:] = self.r_array[self.list_pairs[each][1]]
                Y.append(int(self.list_pairs[each][2]))

        X0 = np.expand_dims(X0, axis=-1)
        X1 = np.expand_dims(X1, axis=-1)
        return X0, X1, Y

class AeToyGenerator(keras.utils.Sequence):
    #TODO: CHECK THIS CAREFULLY
    def __init__(self, array, batch_size, augment = False, dim=(28,28), shuffle = True):
        'Initialization'
        self.batch_size = batch_size
        self.dim = dim
        self.shuffle = shuffle
        self.augment = augment
        self.array = array
        self.on_epoch_end()

    def __len__(self):
        return int(np.floor((len(self.array)) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        batch_indicies = self.indexes[int(index*(self.batch_size)):int((index+1)*(self.batch_size))]
        # Generate data
        X, Y = self.__data_generation(batch_indicies)

        return X, Y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.array))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, batch_indicies):
        'Generates data containing batch_size samples' 
        # Initialization
        #add channels here
        X = np.empty((self.batch_size, *self.dim))
        Y = np.empty((self.batch_size, *self.dim))

        if self.augment:
            rotation_degs = list(np.random.randint(0, 360, self.batch_size))
            for index, each in enumerate(batch_indicies):
                X[index,:,:] = self.array[each]
                Y[index,:,:] = skimage.transform.rotate(self.array[each], rotation_degs[index], resize=False, order=1, preserve_range=True)    
        else:
            for index, each in enumerate(batch_indicies):
                X[index,:,:] = self.array[each]
                Y[index,:,:] = self.array[each]        

        X = np.expand_dims(X, axis=-1)
        Y = np.expand_dims(Y, axis=-1)
        return X, Y

class ToyCompletionGenerator(keras.utils.Sequence):
    #TODO: No jitter yet. at current resolution it makes things extra blurry
    '''
    Note, cannot apply shift if augment is False.
    '''
    def __init__(self, l_array, r_array, batch_size, n_augmentations = 1, dim=(28,28), shuffle = True, augment = False, shift_type = None):
        'Initialization'
        self.batch_size = batch_size
        self.dim = dim
        self.shuffle = shuffle
        self.n_augmentations = n_augmentations
        self.augment = augment
        self.l_array = l_array
        self.r_array = r_array
        self.shift_type = shift_type

        #generate pairs of both lists:
        list_pairs = []

        #REPLACE THIS:
        master_length = len(l_array)
        master_index_list = list(range(master_length))
        data_list= []
        for indicies in master_index_list:
                #append true pair
                data_list.append([indicies,indicies])
        #keeping list of indicies approach for simplicity... add minimal overhead so no worries!
        random.shuffle(data_list)
        self.list_pairs = data_list
        self.on_epoch_end()

    def __len__(self):
        return int(np.floor((len(self.list_pairs) * self.n_augmentations) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        batch_indicies = self.indexes[int(index*(self.batch_size/self.n_augmentations)):int((index+1)*(self.batch_size/self.n_augmentations))]
        # Generate data
        X0, X1, Y = self.__data_generation(batch_indicies)
        if np.isnan(X0).any() or np.isnan(X1).any():
            print('BROKEN')       
            raise SystemExit(0)

        return [X0,X1], Y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_pairs))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def get_coords(self, img_array):
        coords = []
        for i in range(img_array.shape[0]):
            for j in range(img_array.shape[1]):
                if img_array[i][j]:
                    coords.append((i,j))
        return(np.array(coords))

    def shift(self, arr, x_off, y_off, fill_value=0):
        y_shifted_result = np.empty_like(arr)
        x_off = int(x_off)
        y_off = int(y_off)
        #first y_offset
        if y_off > 0:
            y_shifted_result[:y_off] = fill_value
            y_shifted_result[y_off:] = arr[:-y_off]
        elif y_off < 0:
            y_shifted_result[y_off:] = fill_value
            y_shifted_result[:y_off] = arr[-y_off:]
        else:
            y_shifted_result = arr
        
        x_shifted_result = np.empty_like(y_shifted_result)
        if x_off > 0:
            x_shifted_result[:,:x_off] = fill_value
            x_shifted_result[:,x_off:] = y_shifted_result[:,:-x_off]
        elif x_off < 0:
            x_shifted_result[:,x_off:] = fill_value
            x_shifted_result[:,:x_off] = y_shifted_result[:,-x_off:]
        return x_shifted_result

    def center_array(self, input_array, size=28):
        center = self.get_coords(input_array).mean(axis=0)
        offset = np.array([size/2,size/2]) - center
        centered_array = self.shift(input_array, offset[1], offset[0])
        return centered_array

    def __data_generation(self, batch_indicies):
        'Generates data containing batch_size samples' 
        # Initialization
        n_channels = 1
        expanded_dim = self.dim + (n_channels,)
        #add channels here
        X0 = np.empty((self.batch_size, *self.dim))
        X1 = np.empty((self.batch_size, *self.dim))
        Y = np.empty((self.batch_size, *self.dim, 2))
        if self.shift_type not in (None, 'center'):
            raise ValueError('shift_type %s is invalid' % (self.shift_type))     

        if self.augment or self.n_augmentations > 1:
            for index, each in enumerate(batch_indicies):
                l = self.l_array[self.list_pairs[each][0]]
                r = self.r_array[self.list_pairs[each][1]]
                Y_temp = np.stack((l,r),axis=-1)
                if self.shift_type == 'center':
                    l = self.center_array(l, size=self.dim[0])
                    r = self.center_array(r, size=self.dim[0])
                rotation_degs = np.random.randint(0, 360, size=self.n_augmentations)
                for j, degrees in enumerate(list(rotation_degs)):
                  aug_index = (index*self.n_augmentations) + j   
                  X0[aug_index,:,:] = skimage.transform.rotate(l, degrees, resize=False, order=1, preserve_range=True)
                rotation_degs = np.random.randint(0, 360, size=self.n_augmentations)
                for j, degrees in enumerate(list(rotation_degs)):
                  aug_index = (index*self.n_augmentations) + j       
                  X1[aug_index,:,:] = skimage.transform.rotate(r, degrees, resize=False, order=1, preserve_range=True)
                  Y[aug_index,:,:,:] = Y_temp
        else:
            for index, each in enumerate(batch_indicies):
                l = self.l_array[self.list_pairs[each][0]]
                r = self.r_array[self.list_pairs[each][1]]
                Y_temp = np.stack((l,r),axis=-1)

                X0[index,:,:] = l
                X1[index,:,:] = r
                Y[index,:,:,:] = Y_temp

        X0 = np.expand_dims(X0, axis=-1)
        X1 = np.expand_dims(X1, axis=-1)
        return X0, X1, Y

class DeepCompletionGenerator(keras.utils.Sequence):
    #('on ice')
    '''Should we stack voxel grids along new axis, or existing channel axis? First attempt will be along existing axis - otherwise would need 4d upsample?
    '''
    '''
    def __init__(self, log_file, data_directory, batch_size, n_augmentations=1, dim=(32,32,32), vox_size = 0.6, augment = False, shuffle = True, max_one = False, one_channel = False, shared_center = True):
        #further afield... aa level definitions
        #undo size filtering, center crop on CDRs
        #If center is false, center of CDR/AG interface is left as center
        #if the length of a line in the log file is four, detect that there are two centers provided and apply them automatically
        
        'Initialization'
        self.log_file = log_file
        self.data_directory = data_directory.rstrip('/') + str('/')
        self.batch_size = batch_size
        self.dim = dim
        self.vox_size = vox_size
        self.n_augmentations = n_augmentations
        self.shuffle = shuffle
        self.augment = augment
        self.max_one = max_one
        self.one_channel = one_channel
        self.shared_center = shared_center

        #TODO: Add automatic mapping to "Other"
        self.atom_types = ['C', 'H', 'O', 'N', 'P', 'S','Other']
        self.atom_type_key, self.atom_type_encoding = convert_categorical_to_binary(self.atom_types,self.atom_types)

        with open(self.log_file, 'r') as f:
            reader = csv.reader(f)
            data_list = list(reader)

        #preload all data into list like this:
        preloaded_data = []
        for each_line in data_list:
            pdb_code = each_line[0]
            if len(each_line) == 5:
                if shared_center:
                    #center is always processes as list w/ length 3, if shared just duplicate
                    center_coords = [ast.literal_eval(each_line[3]), ast.literal_eval(each_line[3]), ast.literal_eval(each_line[3])]
                else:
                    center_coords = [ast.literal_eval(each_line[1]), ast.literal_eval(each_line[2]), ast.literal_eval(each_line[3])]
            else:
                raise ValueError('Log format is incorrect!')     
            Ab_file = pdb_parser(self.data_directory + pdb_code + "_Ab.pdb")
            Ab_coords = np.array(Ab_file)[:,0:3].astype(np.float)
            Ab_atoms = np.array(Ab_file)[:,3]
            Ag_file = pdb_parser(self.data_directory + pdb_code + "_Ag.pdb")
            Ag_coords = np.array(Ag_file)[:,0:3].astype(np.float)
            Ag_atoms = np.array(Ag_file)[:,3]
            preloaded_data.append([pdb_code,center_coords,Ab_coords,Ab_atoms,Ag_coords,Ag_atoms])
        self.preloaded_data = preloaded_data
        self.on_epoch_end()

    def __len__(self):
        return int(np.floor((len(self.preloaded_data) * self.n_augmentations) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        batch_indicies = self.indexes[int(index*(self.batch_size/self.n_augmentations)):int((index+1)*(self.batch_size/self.n_augmentations))]
        # Generate data
        X0, X1, Y = self.__data_generation(batch_indicies)

        return [X0,X1], Y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.preloaded_data))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, batch_indicies):
        'Generates data containing batch_size samples' 
        # Initialization
        n_channels = len(self.atom_type_encoding)
        expanded_dim = self.dim + (n_channels,)
        expanded_dim_out = self.dim + (2*n_channels,)
        #add channels here
        X0 = np.empty((self.batch_size, *expanded_dim))
        X1 = np.empty((self.batch_size, *expanded_dim))
        #TODO: alternately, try 7?
        Y =  np.empty((self.batch_size, *expanded_dim_out))

        for index_num, single_index in enumerate(batch_indicies):
            pdb_code = self.preloaded_data[single_index][0]
            Ab_coords = self.preloaded_data[single_index][2]
            Ab_atoms = self.preloaded_data[single_index][3]
            #selects first of centers, if there are two or one, working either way
            center_coords = np.array(self.preloaded_data[single_index][1][0])
            #one-hot encode atoms
            Ab_encoded_atoms = np.empty((len(Ab_atoms),n_channels))
            for i, atom in enumerate(Ab_atoms):
                if atom not in self.atom_types:
                    atom = 'Other'
                Ab_encoded_atoms[i,:] = self.atom_type_encoding[self.atom_type_key[atom]]

            #augment coords
            if self.augment or self.n_augmentations > 1:
                augmented_Ab_coordinates, center_array = augment_dataset(Ab_coords, self.n_augmentations, center_coords)
                for i in range(self.n_augmentations):
                    voxelized_ab = voxelize(augmented_Ab_coordinates[i], Ab_encoded_atoms, center=center_array[i], voxel_size = self.vox_size, grid_shape=self.dim)
                    aug_index = (index_num*self.n_augmentations) + i   
                    X0[(aug_index),:] = voxelized_ab
            #no augmenting, just voxelize and add            
            else:
                voxelized_ab = voxelize(Ab_coords, Ab_encoded_atoms, center=center_coords, voxel_size = self.vox_size, grid_shape=self.dim)
                X0[(index_num),:] = voxelized_ab

            #now antigen
            Ag_coords = self.preloaded_data[single_index][4]
            Ag_atoms = self.preloaded_data[single_index][5]
            #selects last of centers, if there are two or one, working either way
            center_coords = np.array(self.preloaded_data[single_index][1][1])
      
            #one-hot encode atoms
            Ag_encoded_atoms = np.empty((len(Ag_atoms),n_channels))
            for i, atom in enumerate(Ag_atoms):
                if atom not in self.atom_types:
                    atom = 'Other'
                Ag_encoded_atoms[i,:] = self.atom_type_encoding[self.atom_type_key[atom]]

            #augment coords
            if self.augment or self.n_augmentations > 1:
                augmented_Ag_coordinates, center_array = augment_dataset(Ag_coords, self.n_augmentations, center_coords)

                for i in range(self.n_augmentations):
                    voxelized_ag = voxelize(augmented_Ag_coordinates[i], Ag_encoded_atoms, center=center_array[i], voxel_size = self.vox_size, grid_shape=self.dim)
                    aug_index = (index_num*self.n_augmentations) + i
                    X1[(aug_index),:] = voxelized_ag

            #no augmenting, just voxelize and add            
            else:
                voxelized_ag = voxelize(Ag_coords, Ag_encoded_atoms, center=center_coords, voxel_size = self.vox_size, grid_shape=self.dim)
                X1[(index_num),:] = voxelized_ag
            
            #now Y:
            center_coords = np.array(self.preloaded_data[single_index][1][2])
            #we already have coords and atoms for both
            if self.augment or self.n_augmentations > 1:
                augmented_Ab_coordinates_for_complex, center_array = augment_dataset(Ab_coords, self.n_augmentations, center_coords)
                augmented_Ag_coordinates_for_complex, center_array = augment_dataset(Ag_coords, self.n_augmentations, center_coords)

                for i in range(self.n_augmentations):
                    voxelized_ab = voxelize(augmented_Ab_coordinates_for_complex[i], Ab_encoded_atoms, center=center_array[i], voxel_size = self.vox_size, grid_shape=self.dim)
                    voxelized_ag = voxelize(augmented_Ag_coordinates_for_complex[i], Ag_encoded_atoms, center=center_array[i], voxel_size = self.vox_size, grid_shape=self.dim)
                    aug_index = (index_num*self.n_augmentations) + i
                    #now merge           
                    Y[(aug_index),:] = np.concatenate((voxelized_ab,voxelized_ag),axis=-1)

            #no augmenting, just voxelize and add            
            else:
                voxelized_ab = voxelize(Ab_coords, Ab_encoded_atoms, center=center_coords, voxel_size = self.vox_size, grid_shape=self.dim)
                voxelized_ag = voxelize(Ag_coords, Ag_encoded_atoms, center=center_coords, voxel_size = self.vox_size, grid_shape=self.dim)
                Y[(index_num),:] = np.concatenate((voxelized_ab,voxelized_ag),axis=-1)
        
        if self.one_channel:
            X0 = X0.sum(axis=-1, keepdims=True)
            X1 = X1.sum(axis=-1, keepdims = True)
            Y = Y.sum(axis=-1, keepdims = True)
        if self.max_one:
            X0 = np.array(X0 != 0).astype(np.int)
            X1 = np.array(X1 != 0).astype(np.int)
            Y = np.array(Y != 0).astype(np.int)
        return X0, X1, Y
        '''

class DeepCompletionGeneratorV2(keras.utils.Sequence):
    '''Accepts preloaded log instead of path -- more flexible for train/test splitting. Old version kept for continuity
    '''
    def __init__(self, log_list, data_directory, batch_size, n_augmentations=1, dim=(32,32,32), vox_size = 0.6, augment = False, shuffle = True, max_one = False, one_channel = False, shared_center = True, empty_channel=False, blur = False, blur_trunc = 4.0):
        #further afield... aa level definitions
        #undo size filtering, center crop on CDRs
        #If center is false, center of CDR/AG interface is left as center
        #if the length of a line in the log file is four, detect that there are two centers provided and apply them automatically
        
        'Initialization'
        self.log_list = log_list
        self.data_directory = data_directory.rstrip('/') + str('/')
        self.batch_size = batch_size
        self.dim = dim
        self.vox_size = vox_size
        self.n_augmentations = n_augmentations
        self.shuffle = shuffle
        self.augment = augment
        self.max_one = max_one
        self.one_channel = one_channel
        self.shared_center = shared_center
        self.empty_channel = empty_channel
        self.blur = blur
        self.blur_trunc = blur_trunc

        #TODO: Add automatic mapping to "Other"
        self.atom_types = ['C', 'H', 'O', 'N', 'P', 'S','Other']
        self.atom_type_key, self.atom_type_encoding = convert_categorical_to_binary(self.atom_types,self.atom_types)

        #preload all data into list like this:
        preloaded_data = []
        for each_line in self.log_list:
            pdb_code = each_line[0]
            if len(each_line) == 5:
                if shared_center:
                    #center is always processes as list w/ length 3, if shared just duplicate
                    center_coords = [ast.literal_eval(each_line[3]), ast.literal_eval(each_line[3]), ast.literal_eval(each_line[3])]
                else:
                    center_coords = [ast.literal_eval(each_line[1]), ast.literal_eval(each_line[2]), ast.literal_eval(each_line[3])]
            else:
                raise ValueError('Log format is incorrect!')     
            Ab_file = pdb_parser(self.data_directory + pdb_code + "_Ab.pdb")
            Ab_coords = np.array(Ab_file)[:,0:3].astype(np.float)
            Ab_atoms = np.array(Ab_file)[:,3]
            Ag_file = pdb_parser(self.data_directory + pdb_code + "_Ag.pdb")
            Ag_coords = np.array(Ag_file)[:,0:3].astype(np.float)
            Ag_atoms = np.array(Ag_file)[:,3]
            preloaded_data.append([pdb_code,center_coords,Ab_coords,Ab_atoms,Ag_coords,Ag_atoms])
        self.preloaded_data = preloaded_data
        self.on_epoch_end()

    def __len__(self):
        return int(np.floor((len(self.preloaded_data) * self.n_augmentations) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        batch_indicies = self.indexes[int(index*(self.batch_size/self.n_augmentations)):int((index+1)*(self.batch_size/self.n_augmentations))]
        # Generate data
        X0, X1, Y = self.__data_generation(batch_indicies)

        return [X0,X1], Y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.preloaded_data))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, batch_indicies):
        'Generates data containing batch_size samples' 
        # Initialization
        n_channels = len(self.atom_type_encoding)
        if self.empty_channel:
            expanded_dim = self.dim + ((1+n_channels),)
            expanded_dim_out = self.dim + ((1+(2*n_channels)),)
        else:
            expanded_dim = self.dim + (n_channels,)
            expanded_dim_out = self.dim + (2*n_channels,)
        #add channels here
        X0 = np.empty((self.batch_size, *expanded_dim))
        X1 = np.empty((self.batch_size, *expanded_dim))
        #TODO: alternately, try 7?
        Y =  np.empty((self.batch_size, *expanded_dim_out))

        for index_num, single_index in enumerate(batch_indicies):
            pdb_code = self.preloaded_data[single_index][0]
            Ab_coords = self.preloaded_data[single_index][2]
            Ab_atoms = self.preloaded_data[single_index][3]
            #selects first of centers, if there are two or one, working either way
            center_coords = np.array(self.preloaded_data[single_index][1][0])
            #one-hot encode atoms
            Ab_encoded_atoms = np.empty((len(Ab_atoms),n_channels))
            for i, atom in enumerate(Ab_atoms):
                if atom not in self.atom_types:
                    atom = 'Other'
                Ab_encoded_atoms[i,:] = self.atom_type_encoding[self.atom_type_key[atom]]

            #augment coords
            if self.augment or self.n_augmentations > 1:
                augmented_Ab_coordinates, center_array = augment_dataset(Ab_coords, self.n_augmentations, center_coords)
                for i in range(self.n_augmentations):
                    voxelized_ab = voxelize(augmented_Ab_coordinates[i], Ab_encoded_atoms, center=center_array[i], voxel_size = self.vox_size, grid_shape=self.dim, empty_channel = self.empty_channel)
                    aug_index = (index_num*self.n_augmentations) + i   
                    X0[(aug_index),:] = voxelized_ab
            #no augmenting, just voxelize and add            
            else:
                voxelized_ab = voxelize(Ab_coords, Ab_encoded_atoms, center=center_coords, voxel_size = self.vox_size, grid_shape=self.dim, empty_channel = self.empty_channel)
                X0[(index_num),:] = voxelized_ab

            #now antigen
            Ag_coords = self.preloaded_data[single_index][4]
            Ag_atoms = self.preloaded_data[single_index][5]
            #selects last of centers, if there are two or one, working either way
            center_coords = np.array(self.preloaded_data[single_index][1][1])
      
            #one-hot encode atoms
            Ag_encoded_atoms = np.empty((len(Ag_atoms),n_channels))
            for i, atom in enumerate(Ag_atoms):
                if atom not in self.atom_types:
                    atom = 'Other'
                Ag_encoded_atoms[i,:] = self.atom_type_encoding[self.atom_type_key[atom]]

            #augment coords
            if self.augment or self.n_augmentations > 1:
                augmented_Ag_coordinates, center_array = augment_dataset(Ag_coords, self.n_augmentations, center_coords)

                for i in range(self.n_augmentations):
                    voxelized_ag = voxelize(augmented_Ag_coordinates[i], Ag_encoded_atoms, center=center_array[i], voxel_size = self.vox_size, grid_shape=self.dim, empty_channel = self.empty_channel)
                    aug_index = (index_num*self.n_augmentations) + i
                    X1[(aug_index),:] = voxelized_ag

            #no augmenting, just voxelize and add            
            else:
                voxelized_ag = voxelize(Ag_coords, Ag_encoded_atoms, center=center_coords, voxel_size = self.vox_size, grid_shape=self.dim, empty_channel = self.empty_channel)
                X1[(index_num),:] = voxelized_ag
            
            #now Y:
            center_coords = np.array(self.preloaded_data[single_index][1][2])
            #we already have coords and atoms for both
            if self.augment or self.n_augmentations > 1:
                augmented_Ab_coordinates_for_complex, center_array = augment_dataset(Ab_coords, self.n_augmentations, center_coords)
                augmented_Ag_coordinates_for_complex, center_array = augment_dataset(Ag_coords, self.n_augmentations, center_coords)

                #generate Y from unperturbed/augmented components:
                voxelized_ab_no_aug = voxelize(Ab_coords, Ab_encoded_atoms, center=center_coords, voxel_size = self.vox_size, grid_shape=self.dim, empty_channel = self.empty_channel)
                voxelized_ag_no_aug = voxelize(Ag_coords, Ag_encoded_atoms, center=center_coords, voxel_size = self.vox_size, grid_shape=self.dim, empty_channel = self.empty_channel)
                
                for i in range(self.n_augmentations):
                    voxelized_ab = voxelize(augmented_Ab_coordinates_for_complex[i], Ab_encoded_atoms, center=center_array[i], voxel_size = self.vox_size, grid_shape=self.dim, empty_channel = self.empty_channel)
                    voxelized_ag = voxelize(augmented_Ag_coordinates_for_complex[i], Ag_encoded_atoms, center=center_array[i], voxel_size = self.vox_size, grid_shape=self.dim, empty_channel = self.empty_channel)
                    aug_index = (index_num*self.n_augmentations) + i
                    #now merge...BUG! this is not the way to return the complex, it is just a mashup of the two augmented structures
                    #concatenating doesnt work if we want to share an empty channel, which we do
                    if self.empty_channel:
                        Y[(aug_index),:] = np.concatenate(((voxelized_ab_no_aug[:,:,:,0:1]*voxelized_ab_no_aug[:,:,:,0:1]),voxelized_ab_no_aug[:,:,:,1:],voxelized_ag_no_aug[:,:,:,1:]), axis = -1)
                    else:           
                        Y[(aug_index),:] = np.concatenate((voxelized_ab_no_aug,voxelized_ag_no_aug),axis=-1)

            #no augmenting, just voxelize and add            
            else:
                voxelized_ab = voxelize(Ab_coords, Ab_encoded_atoms, center=center_coords, voxel_size = self.vox_size, grid_shape=self.dim, empty_channel = self.empty_channel)
                voxelized_ag = voxelize(Ag_coords, Ag_encoded_atoms, center=center_coords, voxel_size = self.vox_size, grid_shape=self.dim, empty_channel = self.empty_channel)
                if self.empty_channel:
                    Y[(index_num),:] = np.concatenate(((voxelized_ab[:,:,:,0:1]*voxelized_ag[:,:,:,0:1]),voxelized_ab[:,:,:,1:],voxelized_ag[:,:,:,1:]),axis=-1)
                else:
                    Y[(index_num),:] = np.concatenate((voxelized_ab,voxelized_ag),axis=-1)
        
        if self.one_channel:
            X0 = X0.sum(axis=-1, keepdims=True)
            X1 = X1.sum(axis=-1, keepdims = True)
            Y = Y.sum(axis=-1, keepdims = True)
        if self.max_one:
            X0 = np.array(X0 != 0).astype(np.int)
            X1 = np.array(X1 != 0).astype(np.int)
            Y = np.array(Y != 0).astype(np.int)

        if self.blur != False:
            X0 = X0.astype(float)
            X1 = X1.astype(float)
            Y = Y.astype(float)
            X0 = scipy.ndimage.gaussian_filter(X0, [0,self.blur,self.blur,self.blur,0], truncate = self.blur_trunc)
            X1 = scipy.ndimage.gaussian_filter(X1, [0,self.blur,self.blur,self.blur,0], truncate = self.blur_trunc)
            Y = scipy.ndimage.gaussian_filter(Y, [0,self.blur,self.blur,self.blur,0], truncate = self.blur_trunc)

        if self.blur != False and self.empty_channel:
            X0[...,0] =(X0[:,:,:,:,1:].sum(axis=(4)) == 0).astype(float)
            X1[...,0] =(X1[:,:,:,:,1:].sum(axis=(4)) == 0).astype(float)
            Y[...,0] =(Y[:,:,:,:,1:].sum(axis=(4)) == 0).astype(float)

        return X0, X1, Y

class VoxelAEGenerator(keras.utils.Sequence):
    '''
    '''
    def calc_center(self, input_array):
        array_center = np.mean((input_array.max(axis=0), input_array.min(axis=0)), axis = 0)
        return array_center

    def __init__(self, file_list, batch_size, n_augmentations=1, augment = False, dim=(64,64,64), voxel_size=0.6, shuffle = False, check_sizes = False, blur = False, blur_trunc = 4.0, empty_channel = False):
        'Initialization'
        self.file_list = file_list
        self.batch_size = batch_size
        self.dim = dim
        self.voxel_size = voxel_size
        self.n_augmentations = n_augmentations
        self.augment = augment
        self.shuffle = shuffle
        self.blur = blur
        self.blur_trunc = blur_trunc
        self.empty_channel = empty_channel

        #TODO: Add automatic mapping to "Other"
        self.atom_types = ['C', 'H', 'O', 'N', 'P', 'S','Other']
        self.atom_type_key, self.atom_type_encoding = convert_categorical_to_binary(self.atom_types,self.atom_types)

        if n_augmentations >  batch_size:
            raise ValueError('n_augmentations cannot be larger than the batch size!')

        extensions = set([i.split('.')[-1] for i in self.file_list])
        if len(extensions) > 1:
            raise ValueError('More than one type of file found in input directory!')
        elif list(extensions)[0] == 'sdf':
            self.format_to_parse = 'sdf'
        elif list(extensions)[0] == 'pdb':
            self.format_to_parse = 'pdb'
        else:
            print(list(extensions)[0])
            raise ValueError('Input files were not PDB or SDF!')
        
        if check_sizes:
            distance_constraint = dim[0] * voxel_size
            filtered_list = []
            for each in self.file_list:
                if self.format_to_parse == 'sdf':
                    single_input_file = sdf_parser(each)
                elif self.format_to_parse == 'pdb':
                    single_input_file = pdb_parser(each)
                coords_to_check = np.array(single_input_file)[:,0:3].astype(np.float)
                center_to_check = self.calc_center(coords_to_check)
                max_distance = np.linalg.norm((center_to_check - coords_to_check), axis = -1).max()
                if max_distance < distance_constraint:
                    filtered_list.append(each)
            print('Removed %d files which did not fit in the voxel grid' % (len(self.file_list) - len(filtered_list)))  
            self.file_list = filtered_list      
        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor((len(self.file_list) * self.n_augmentations) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        batch_indicies = self.indexes[int(index*(self.batch_size/self.n_augmentations)):int((index+1)*(self.batch_size/self.n_augmentations))]
        # Generate data
        X = self.__data_generation(batch_indicies)

        return X, X

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.file_list)*self.n_augmentations)
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, batch_indicies):
        'Generates data containing batch_size samples' 
        # Initialization
        n_channels = len(self.atom_type_encoding)
        if self.empty_channel:
            expanded_dim = self.dim + ((1+n_channels),)
        else:
            expanded_dim = self.dim + (n_channels,)
        #add channels here
        X = np.empty((self.batch_size, *expanded_dim))
        for index_num, each in enumerate(batch_indicies):
            #open/read sdf file and get center
            if self.format_to_parse == 'sdf':
                single_input_file = sdf_parser(self.file_list[each])
            elif self.format_to_parse == 'pdb':
                single_input_file = pdb_parser(self.file_list[each])
            input_coords = np.array(single_input_file)[:,0:3].astype(np.float)
            input_atoms = np.array(single_input_file)[:,3]
            center_coords = self.calc_center(input_coords)
      
            #one-hot encode atoms
            encoded_atoms = np.empty((len(input_atoms),n_channels))
            for i, atom in enumerate(input_atoms):
                if atom not in self.atom_types:
                    atom = 'Other'
                encoded_atoms[i,:] = self.atom_type_encoding[self.atom_type_key[atom]]

            if self.augment or self.n_augmentations > 1:
                rotation_vectors = np.random.randint(0, 359, (self.n_augmentations,3))
                augmented_coordinates, center_array = augment_predefined_angles(input_coords, rotation_vectors, center_coords)

                for i in range(self.n_augmentations):
                    aug_index = (index_num*self.n_augmentations) + i
                    voxelized_file = voxelize(augmented_coordinates[i], encoded_atoms, center=center_array[i], voxel_size = self.voxel_size, grid_shape=self.dim, empty_channel = self.empty_channel)
                    X[(aug_index),:] = voxelized_file
            else:
                voxelized_file = voxelize(input_coords, encoded_atoms, center=center_coords, voxel_size = self.voxel_size, grid_shape=self.dim, empty_channel = self.empty_channel)
                X[(index_num),:] = voxelized_file
            
        if self.blur != False:
            X = scipy.ndimage.gaussian_filter(X, [0,self.blur,self.blur,self.blur,0], truncate = self.blur_trunc)
        if self.blur != False and self.empty_channel:
            X[...,0] =(X[:,:,:,:,1:].sum(axis=(4)) == 0).astype(float)

        return X

class SmallMoleculeBinaryDock(keras.utils.Sequence):
    '''
    should load list of small molecules and proteins, use proper pairing to frame protein active site
    assume gan-like file naming structure (still need to dl from pdbbind)

    check for small molecule fit within voxel grid

    shuffle and randomly augment
    '''
    def calc_center(self, input_array):
        array_center = np.mean((input_array.max(axis=0), input_array.min(axis=0)), axis = 0)
        return array_center

    def __init__(self, file_list, batch_size, n_augmentations=1, augment = False, dim=(64,64,64), voxel_size=0.6, shuffle = False, check_sizes = True, blur = False, blur_trunc = 4.0, empty_channel = False, max_one = False, one_channel = False, margin = 5, max_imbalance = 5):
        'Initialization'
        self.file_list = file_list # list PDB codes, no extensions
        self.batch_size = batch_size
        self.dim = dim
        self.voxel_size = voxel_size
        self.n_augmentations = n_augmentations
        self.augment = augment
        self.shuffle = shuffle
        self.blur = blur
        self.blur_trunc = blur_trunc
        self.empty_channel = empty_channel
        self.max_imbalance = max_imbalance
        self.max_one = max_one
        self.one_channel = one_channel

        self.atom_types = ['C', 'H', 'O', 'N', 'P', 'S','Other']
        self.atom_type_key, self.atom_type_encoding = convert_categorical_to_binary(self.atom_types,self.atom_types)

        self.ligand_list = [ file + str('_ligand.sdf') for file in file_list ]

        if n_augmentations >  batch_size:
            raise ValueError('n_augmentations cannot be larger than the batch size!')

        if check_sizes:
            distance_constraint = dim[0] * voxel_size
            filtered_ligand_list = []
            for each in self.ligand_list:
                single_input_file = sdf_parser(each)
                coords_to_check = np.array(single_input_file)[:,0:3].astype(np.float)
                center_to_check = self.calc_center(coords_to_check)
                max_distance = np.linalg.norm((center_to_check - coords_to_check), axis = -1).max()
                if max_distance + margin  < distance_constraint:
                    filtered_ligand_list.append([each, each.replace('_ligand.sdf', '_protein.pdb'), center_to_check])
            print('Removed %d files because the ligands did not fit in the voxel grid' % (len(self.ligand_list) - len(filtered_ligand_list)))
            random.shuffle(filtered_ligand_list)
            self.filtered_ligand_list = filtered_ligand_list
        #should master list be made in this process? e.g. [ligand,protein, center]

        list_pairs = []
        master_length = len(self.filtered_ligand_list)
        master_index_list = list(range(master_length))
        data_list = []
        for indicies in master_index_list:
            #true data
            data_list.append([self.filtered_ligand_list[indicies][0],self.filtered_ligand_list[indicies][2],self.filtered_ligand_list[indicies][1],self.filtered_ligand_list[indicies][2],1])
            #append next n entries as negative pairs
            for false_num in range(1,self.max_imbalance+1):
                false_index = indicies + false_num
                if false_index >= master_length:
                    false_index = false_index%master_length
                data_list.append([self.filtered_ligand_list[indicies][0],self.filtered_ligand_list[indicies][2],self.filtered_ligand_list[false_index][1],self.filtered_ligand_list[false_index][2],0])
            
        random.shuffle(data_list)
        self.data_list = data_list
        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor((len(self.data_list) * self.n_augmentations) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        batch_indicies = self.indexes[int(index*(self.batch_size/self.n_augmentations)):int((index+1)*(self.batch_size/self.n_augmentations))]
        # Generate data
        [P,L], Y = self.__data_generation(batch_indicies)

        return [P,L], Y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.data_list)*self.n_augmentations)
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, batch_indicies):
        'Generates data containing batch_size samples' 
        # Initialization
        n_channels = len(self.atom_type_encoding)
        if self.empty_channel:
            expanded_dim = self.dim + ((1+n_channels),)
        else:
            expanded_dim = self.dim + (n_channels,)
        #add channels here
        P = np.empty((self.batch_size, *expanded_dim))
        L = np.empty((self.batch_size, *expanded_dim))
        Y = []

        for index_num, each in enumerate(batch_indicies):
            #open/read sdf file and get center
            sdf_input = sdf_parser(self.data_list[each][0])
            sdf_center_coords = self.data_list[each][1]
            pdb_input = pdb_parser(self.data_list[each][2])
            pdb_center_coords = self.data_list[each][3]
            label = self.data_list[each][4]
            sdf_input_coords = np.array(sdf_input)[:,0:3].astype(np.float)
            sdf_input_atoms = np.array(sdf_input)[:,3]
            pdb_input_coords = np.array(pdb_input)[:,0:3].astype(np.float)
            pdb_input_atoms = np.array(pdb_input)[:,3]
      
            #one-hot encode atoms
            sdf_encoded_atoms = np.empty((len(sdf_input_atoms),n_channels))
            for i, atom in enumerate(sdf_input_atoms):
                if atom not in self.atom_types:
                    atom = 'Other'
                sdf_encoded_atoms[i,:] = self.atom_type_encoding[self.atom_type_key[atom]]

            pdb_encoded_atoms = np.empty((len(pdb_input_atoms),n_channels))
            for i, atom in enumerate(pdb_input_atoms):
                if atom not in self.atom_types:
                    atom = 'Other'
                pdb_encoded_atoms[i,:] = self.atom_type_encoding[self.atom_type_key[atom]]

            if self.augment or self.n_augmentations > 1:
                sdf_rotation_vectors = np.random.randint(0, 359, (self.n_augmentations,3))
                sdf_augmented_coordinates, sdf_center_array = augment_predefined_angles(sdf_input_coords, sdf_rotation_vectors, sdf_center_coords)
                pdb_rotation_vectors = np.random.randint(0, 359, (self.n_augmentations,3))
                pdb_augmented_coordinates, pdb_center_array = augment_predefined_angles(pdb_input_coords, pdb_rotation_vectors, pdb_center_coords)

                for i in range(self.n_augmentations):
                    aug_index = (index_num*self.n_augmentations) + i
                    #voxelize sdf
                    voxelized_file = voxelize(sdf_augmented_coordinates[i], sdf_encoded_atoms, center=sdf_center_array[i], voxel_size = self.voxel_size, grid_shape=self.dim, empty_channel = self.empty_channel)
                    L[(aug_index),:] = voxelized_file

                    #voxelize pdb
                    voxelized_file = voxelize(pdb_augmented_coordinates[i], pdb_encoded_atoms, center=pdb_center_array[i], voxel_size = self.voxel_size, grid_shape=self.dim, empty_channel = self.empty_channel)
                    P[(aug_index),:] = voxelized_file
                    Y.append(label)
            else:
                voxelized_file = voxelize(sdf_input_coords, sdf_encoded_atoms, center=sdf_center_coords, voxel_size = self.voxel_size, grid_shape=self.dim, empty_channel = self.empty_channel)
                L[(index_num),:] = voxelized_file

                voxelized_file = voxelize(pdb_input_coords, pdb_encoded_atoms, center=pdb_center_coords, voxel_size = self.voxel_size, grid_shape=self.dim, empty_channel = self.empty_channel)
                P[(index_num),:] = voxelized_file
                Y.append(label)

        if self.one_channel:
            P = P.sum(axis=-1, keepdims=True)
            L = L.sum(axis=-1, keepdims = True)

        if self.max_one:
            P = np.array(P != 0).astype(np.int)
            L = np.array(L != 0).astype(np.int)

        if self.blur != False:
            P = P.astype(float)
            L = L.astype(float)
            P = scipy.ndimage.gaussian_filter(P, [0,self.blur,self.blur,self.blur,0], truncate = self.blur_trunc)
            L = scipy.ndimage.gaussian_filter(L, [0,self.blur,self.blur,self.blur,0], truncate = self.blur_trunc)

        if self.blur != False and self.empty_channel:
            P[...,0] =(P[:,:,:,:,1:].sum(axis=(4)) == 0).astype(float)
            L[...,0] =(L[:,:,:,:,1:].sum(axis=(4)) == 0).astype(float)

        Y = np.array(Y)
        return [P,L], Y

#Working well, checked in detail, just visualize output to be sure

class learnConformerEquivalence(keras.utils.Sequence):
    '''
    should load list of small molecules and proteins, use proper pairing to frame protein active site
    assume gan-like file naming structure (still need to dl from pdbbind)

    check for small molecule fit within voxel grid

    shuffle and randomly augment

    NO IMBALANCE AS OF NOW
    assumes all inputs have at least as many conformers as the first in list
    '''
    def calc_center(self, input_array):
        array_center = np.mean((input_array.max(axis=0), input_array.min(axis=0)), axis = 0)
        return array_center

    def __init__(self, file_list, batch_size, dim=(64,64,64), voxel_size=0.6, shuffle = True, blur = False, blur_trunc = 4.0, empty_channel = False, margin = 5, check_sizes = True ):
        'Initialization'
        self.file_list = file_list # list PDB codes, no extensions
        self.batch_size = batch_size
        self.dim = dim
        self.voxel_size = voxel_size
        self.shuffle = shuffle
        self.blur = blur
        self.blur_trunc = blur_trunc
        self.empty_channel = empty_channel
        self.number_confs = len(glob.glob(file_list[0] + str('_*.sdf')))
        self.check_sizes = check_sizes

        if self.batch_size % self.number_confs != 0:
            raise ValueError('batch size must be evenly divisible by the number of confs')

        self.atom_types = ['C', 'H', 'O', 'N', 'P', 'S','Other']
        self.atom_type_key, self.atom_type_encoding = convert_categorical_to_binary(self.atom_types,self.atom_types)

        #check size is hard coded on - checking conformer 1
        distance_constraint = dim[0] * voxel_size
        filtered_ligand_list = []

        if self.check_sizes:
            for each in self.file_list:
                single_input_file = sdf_parser(each + str('_1.sdf'))
                coords_to_check = np.array(single_input_file)[:,0:3].astype(np.float)
                center_to_check = self.calc_center(coords_to_check)
                max_distance = np.linalg.norm((center_to_check - coords_to_check), axis = -1).max()
                if max_distance + margin  < distance_constraint:
                    filtered_ligand_list.append(each)
            print('Removed %d files because the ligands did not fit in the voxel grid' % (len(self.file_list) - len(filtered_ligand_list)))
            self.filtered_ligand_list = filtered_ligand_list
        else:
            self.filtered_ligand_list = self.file_list

        random.shuffle(self.filtered_ligand_list)
        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor((self.number_confs*2*len(self.filtered_ligand_list)) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        batch_indicies = self.indexes[int(index*(self.batch_size/(self.number_confs*2))):int((index+1)*(self.batch_size/(self.number_confs*2)))]
        # Generate data
        [L1,L2], Y = self.__data_generation(batch_indicies)

        return [L1,L2], Y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.filtered_ligand_list))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, batch_indicies):
        'Generates data containing batch_size samples' 
        # Initialization
        n_channels = len(self.atom_type_encoding)
        if self.empty_channel:
            expanded_dim = self.dim + ((1+n_channels),)
        else:
            expanded_dim = self.dim + (n_channels,)
        #add channels here
        L1 = np.empty((self.batch_size, *expanded_dim))
        L2 = np.empty((self.batch_size, *expanded_dim))
        Y = np.empty(self.batch_size)

        #for each batch_indicie append TWO entries
        for index_num, each in enumerate(batch_indicies):
            #core conf
            core_conf = random.randint(1,self.number_confs)
            mismatched_confs = np.random.randint(1,int(self.number_confs)+1,self.number_confs)
            sdf_input_match_1 = sdf_parser(self.filtered_ligand_list[each] + '_' + str(core_conf) + '.sdf')
            sdf_input_match_1_coords = np.array(sdf_input_match_1)[:,0:3].astype(np.float)
            sdf_input_match_1_center = self.calc_center(sdf_input_match_1_coords)
            sdf_input_match_1_atoms = np.array(sdf_input_match_1)[:,3]

            sdf_input_match_1_encoded_atoms = np.empty((len(sdf_input_match_1_atoms),n_channels))
            for i, atom in enumerate(sdf_input_match_1_atoms):
                if atom not in self.atom_types:
                    atom = 'Other'
                sdf_input_match_1_encoded_atoms[i,:] = self.atom_type_encoding[self.atom_type_key[atom]]

            #get all the other conformers
            for conf_number in list(range(1,self.number_confs+1)):
                sdf_input_match_2 = sdf_parser(self.filtered_ligand_list[each]+ '_' + str(conf_number) + '.sdf')
                alternate_index = (each+conf_number)%len(self.filtered_ligand_list)
                sdf_input_not_match = sdf_parser(self.filtered_ligand_list[alternate_index] + '_' + str(mismatched_confs[conf_number-1]) + '.sdf')


                sdf_input_match_2_coords = np.array(sdf_input_match_2)[:,0:3].astype(np.float)
                sdf_input_match_2_center = self.calc_center(sdf_input_match_2_coords)
                sdf_input_match_2_atoms = np.array(sdf_input_match_2)[:,3]

                sdf_input_match_2_encoded_atoms = np.empty((len(sdf_input_match_2_atoms),n_channels))
                for i, atom in enumerate(sdf_input_match_2_atoms):
                    if atom not in self.atom_types:
                        atom = 'Other'
                    sdf_input_match_2_encoded_atoms[i,:] = self.atom_type_encoding[self.atom_type_key[atom]]

                #three
                sdf_input_not_match_coords = np.array(sdf_input_not_match)[:,0:3].astype(np.float)
                sdf_input_not_match_center = self.calc_center(sdf_input_not_match_coords)
                sdf_input_not_match_atoms = np.array(sdf_input_not_match)[:,3]

                sdf_input_not_match_encoded_atoms = np.empty((len(sdf_input_not_match_atoms),n_channels))
                for i, atom in enumerate(sdf_input_not_match_atoms):
                    if atom not in self.atom_types:
                        atom = 'Other'
                    sdf_input_not_match_encoded_atoms[i,:] = self.atom_type_encoding[self.atom_type_key[atom]]            


                sdf_input_match_1_augmented_coordinates, sdf_input_match_1_center_array = augment_dataset(sdf_input_match_1_coords, 1, sdf_input_match_1_center)
                voxelized_sdf_input_match_1 = voxelize(sdf_input_match_1_augmented_coordinates[0], sdf_input_match_1_encoded_atoms, center=sdf_input_match_1_center_array[0], voxel_size = self.voxel_size, grid_shape=self.dim, empty_channel = self.empty_channel)
                L1[(conf_number-1),:] = voxelized_sdf_input_match_1
                L1[int((self.batch_size/2)+conf_number-1),:] = voxelized_sdf_input_match_1

                sdf_input_match_2_augmented_coordinates, sdf_input_match_2_center_array = augment_dataset(sdf_input_match_2_coords, 1, sdf_input_match_2_center)
                voxelized_sdf_input_match_2 = voxelize(sdf_input_match_2_augmented_coordinates[0], sdf_input_match_2_encoded_atoms, center=sdf_input_match_2_center_array[0], voxel_size = self.voxel_size, grid_shape=self.dim, empty_channel = self.empty_channel)
                L2[(conf_number-1),:] = voxelized_sdf_input_match_2

                sdf_input_not_match_augmented_coordinates, sdf_input_not_match_center_array = augment_dataset(sdf_input_not_match_coords, 1, sdf_input_not_match_center)
                voxelized_sdf_input_not_match = voxelize(sdf_input_not_match_augmented_coordinates[0], sdf_input_not_match_encoded_atoms, center=sdf_input_not_match_center_array[0], voxel_size = self.voxel_size, grid_shape=self.dim, empty_channel = self.empty_channel)
                L2[int((self.batch_size/2)+conf_number-1),:] = voxelized_sdf_input_not_match

                Y[int(conf_number-1)] = 1
                Y[int((self.batch_size/2)+conf_number-1)] = 0

        if self.blur != False:
            L1 = L1.astype(float)
            L2 = L2.astype(float)
            L1 = scipy.ndimage.gaussian_filter(L1, [0,self.blur,self.blur,self.blur,0], truncate = self.blur_trunc)
            L2 = scipy.ndimage.gaussian_filter(L2, [0,self.blur,self.blur,self.blur,0], truncate = self.blur_trunc)

        if self.blur != False and self.empty_channel:
            L1[...,0] =(L1[:,:,:,:,1:].sum(axis=(4)) == 0).astype(float)
            L2[...,0] =(L2[:,:,:,:,1:].sum(axis=(4)) == 0).astype(float)

        Y = np.array(Y)
        return [L1,L2], Y

#mine harder examples!
class learnConformerEquivalenceHarder(keras.utils.Sequence):
    ''''''
    def calc_center(self, input_array):
        array_center = np.mean((input_array.max(axis=0), input_array.min(axis=0)), axis = 0)
        return array_center

    def __init__(self, data_dir, example_map, batch_size,  dim=(32,32,32), voxel_size=0.6, shuffle = True, blur = False, blur_trunc = 4.0, empty_channel = False, margin = 5, check_sizes = True ):
        'Initialization'
        self.data_dir = data_dir # list PDB codes, no extensions
        self.example_map = np.load(example_map)
        self.batch_size = batch_size
        self.dim = dim
        self.voxel_size = voxel_size
        self.shuffle = shuffle
        self.blur = blur
        self.blur_trunc = blur_trunc
        self.empty_channel = empty_channel
        self.check_sizes = check_sizes

        self.atom_types = ['C', 'H', 'O', 'N', 'P', 'S','Other']
        self.atom_type_key, self.atom_type_encoding = convert_categorical_to_binary(self.atom_types,self.atom_types)

        #populate self.file_list with data_dir + file names from map
        file_names = list(self.example_map[:,0])
        file_names = [ self.data_dir + '_'.join(str(x).split('_')[:-1]) for x in file_names ]
        self.number_confs = len(glob.glob(file_names[0] + str('_*.sdf')))
        if self.batch_size % self.number_confs != 0:
            raise ValueError('batch size must be evenly divisible by the number of confs')

        #check size is hard coded on - checking conformer 1
        distance_constraint = dim[0] * voxel_size
        filtered_ligand_list = []

        if self.check_sizes:
            for each in file_names:
                single_input_file = sdf_parser(each + str('_1.sdf'))
                coords_to_check = np.array(single_input_file)[:,0:3].astype(np.float)
                center_to_check = self.calc_center(coords_to_check)
                max_distance = np.linalg.norm((center_to_check - coords_to_check), axis = -1).max()
                if max_distance + margin  < distance_constraint:
                    filtered_ligand_list.append(each)
            print('Removed %d files because the ligands did not fit in the voxel grid' % (len(self.file_list) - len(filtered_ligand_list)))
            self.filtered_ligand_list = filtered_ligand_list
        else:
            self.filtered_ligand_list = file_names

        random.shuffle(self.filtered_ligand_list)
        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor((self.number_confs*2*len(self.filtered_ligand_list)) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        batch_indicies = self.indexes[int(index*(self.batch_size/(self.number_confs*2))):int((index+1)*(self.batch_size/(self.number_confs*2)))]
        # Generate data
        [L1,L2], Y = self.__data_generation(batch_indicies)

        return [L1,L2], Y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.filtered_ligand_list))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, batch_indicies):
        'Generates data containing batch_size samples' 
        # Initialization
        n_channels = len(self.atom_type_encoding)
        if self.empty_channel:
            expanded_dim = self.dim + ((1+n_channels),)
        else:
            expanded_dim = self.dim + (n_channels,)
        #add channels here
        L1 = np.empty((self.batch_size, *expanded_dim))
        L2 = np.empty((self.batch_size, *expanded_dim))
        Y = np.empty(self.batch_size)

        #for each batch_indicie append TWO entries
        for index_num, each in enumerate(batch_indicies):
            #core conf
            core_conf = random.randint(1,self.number_confs)
            #redo this to handle NO replacement
            #mismatched_confs = list(np.random.choice(np.arange(1,int(self.number_confs)+1), self.number_confs, replace=False))
            sdf_input_match_1 = sdf_parser(self.filtered_ligand_list[each] + '_' + str(core_conf) + '.sdf')
            sdf_input_match_1_coords = np.array(sdf_input_match_1)[:,0:3].astype(np.float)
            sdf_input_match_1_center = self.calc_center(sdf_input_match_1_coords)
            sdf_input_match_1_atoms = np.array(sdf_input_match_1)[:,3]

            sdf_input_match_1_encoded_atoms = np.empty((len(sdf_input_match_1_atoms),n_channels))
            for i, atom in enumerate(sdf_input_match_1_atoms):
                if atom not in self.atom_types:
                    atom = 'Other'
                sdf_input_match_1_encoded_atoms[i,:] = self.atom_type_encoding[self.atom_type_key[atom]]

            ligand_name = self.filtered_ligand_list[each].split('/')[-1] + '_30'
            #using the current ligand as query, lookup 5 closest examples to current ligand, and randomly select one (0th is self)
            hard_example = self.example_map[np.where(self.example_map[:,0]==ligand_name)[0][0]][1][random.randint(1,5)][0].decode("utf-8") 
            hard_example = '_'.join(hard_example.split('_')[:-1])

            #get all the other conformers
            for conf_number in list(range(1,self.number_confs+1)):
                sdf_input_match_2 = sdf_parser(self.filtered_ligand_list[each]+ '_' + str(conf_number) + '.sdf')
                sdf_input_not_match = sdf_parser(self.data_dir + hard_example + '_' + str(conf_number) + '.sdf')

                sdf_input_match_2_coords = np.array(sdf_input_match_2)[:,0:3].astype(np.float)
                sdf_input_match_2_center = self.calc_center(sdf_input_match_2_coords)
                sdf_input_match_2_atoms = np.array(sdf_input_match_2)[:,3]

                sdf_input_match_2_encoded_atoms = np.empty((len(sdf_input_match_2_atoms),n_channels))
                for i, atom in enumerate(sdf_input_match_2_atoms):
                    if atom not in self.atom_types:
                        atom = 'Other'
                    sdf_input_match_2_encoded_atoms[i,:] = self.atom_type_encoding[self.atom_type_key[atom]]

                #three
                sdf_input_not_match_coords = np.array(sdf_input_not_match)[:,0:3].astype(np.float)
                sdf_input_not_match_center = self.calc_center(sdf_input_not_match_coords)
                sdf_input_not_match_atoms = np.array(sdf_input_not_match)[:,3]

                sdf_input_not_match_encoded_atoms = np.empty((len(sdf_input_not_match_atoms),n_channels))
                for i, atom in enumerate(sdf_input_not_match_atoms):
                    if atom not in self.atom_types:
                        atom = 'Other'
                    sdf_input_not_match_encoded_atoms[i,:] = self.atom_type_encoding[self.atom_type_key[atom]]            


                sdf_input_match_1_augmented_coordinates, sdf_input_match_1_center_array = augment_dataset(sdf_input_match_1_coords, 1, sdf_input_match_1_center)
                voxelized_sdf_input_match_1 = voxelize(sdf_input_match_1_augmented_coordinates[0], sdf_input_match_1_encoded_atoms, center=sdf_input_match_1_center_array[0], voxel_size = self.voxel_size, grid_shape=self.dim, empty_channel = self.empty_channel)
                L1[(conf_number-1),:] = voxelized_sdf_input_match_1
                L1[int((self.batch_size/2)+conf_number-1),:] = voxelized_sdf_input_match_1

                sdf_input_match_2_augmented_coordinates, sdf_input_match_2_center_array = augment_dataset(sdf_input_match_2_coords, 1, sdf_input_match_2_center)
                voxelized_sdf_input_match_2 = voxelize(sdf_input_match_2_augmented_coordinates[0], sdf_input_match_2_encoded_atoms, center=sdf_input_match_2_center_array[0], voxel_size = self.voxel_size, grid_shape=self.dim, empty_channel = self.empty_channel)
                L2[(conf_number-1),:] = voxelized_sdf_input_match_2

                sdf_input_not_match_augmented_coordinates, sdf_input_not_match_center_array = augment_dataset(sdf_input_not_match_coords, 1, sdf_input_not_match_center)
                voxelized_sdf_input_not_match = voxelize(sdf_input_not_match_augmented_coordinates[0], sdf_input_not_match_encoded_atoms, center=sdf_input_not_match_center_array[0], voxel_size = self.voxel_size, grid_shape=self.dim, empty_channel = self.empty_channel)
                L2[int((self.batch_size/2)+conf_number-1),:] = voxelized_sdf_input_not_match

                Y[int(conf_number-1)] = 1
                Y[int((self.batch_size/2)+conf_number-1)] = 0

        if self.blur != False:
            L1 = L1.astype(float)
            L2 = L2.astype(float)
            L1 = scipy.ndimage.gaussian_filter(L1, [0,self.blur,self.blur,self.blur,0], truncate = self.blur_trunc)
            L2 = scipy.ndimage.gaussian_filter(L2, [0,self.blur,self.blur,self.blur,0], truncate = self.blur_trunc)

        if self.blur != False and self.empty_channel:
            L1[...,0] =(L1[:,:,:,:,1:].sum(axis=(4)) == 0).astype(float)
            L2[...,0] =(L2[:,:,:,:,1:].sum(axis=(4)) == 0).astype(float)

        Y = np.array(Y)
        return [L1,L2], Y
