#python libraries
import copy
import ntpath
import random
import glob
#3rd party libraries
import numpy as np
from tensorflow import keras 
import skimage
from skimage import transform
#custom libraries
from MoleculeCompletion.Utils.featurizer import voxelize, augment_predefined_angles, convert_categorical_to_binary
from MoleculeCompletion.Utils.file_parser import pdb_parser, sdf_parser
#TODO: Fix augmentation counter, add better center calculating logic
class DataGenerator(keras.utils.Sequence):
    '''
    '''
    def calc_center(self, input_array):
        array_center = np.mean((input_array.max(axis=0), input_array.min(axis=0)), axis = 0)
        return array_center

    def __init__(self, directory, batch_size, n_augmentations=1, dim=(64,64,64), voxel_size=1., shuffle = False, check_sizes = True, margin = 5):
        'Initialization'
        self.directory = directory
        self.batch_size = batch_size
        self.dim = dim
        self.voxel_size = voxel_size
        self.n_augmentations = n_augmentations
        self.shuffle = shuffle
        self.check_sizes = check_sizes

        #TODO: Add automatic mapping to "Other"
        self.atom_types = ['C', 'H', 'O', 'N', 'P', 'S','Other']
        self.atom_type_key, self.atom_type_encoding = convert_categorical_to_binary(self.atom_types,self.atom_types)
        ligand_path = self.directory.rstrip('/') + str('/*.sdf')

        self.ligand_list = glob.glob(ligand_path)

        distance_constraint = dim[0] * voxel_size
        filtered_ligand_list = []
        if self.check_sizes:
            for each in self.ligand_list:
                single_input_file = sdf_parser(each)
                coords_to_check = np.array(single_input_file)[:,0:3].astype(np.float)
                center_to_check = self.calc_center(coords_to_check)
                max_distance = np.linalg.norm((center_to_check - coords_to_check), axis = -1).max()
                if max_distance + margin  < distance_constraint:
                    filtered_ligand_list.append(each)
        print('Removed %d files because the ligands did not fit in the voxel grid' % (len(self.ligand_list) - len(filtered_ligand_list)))
        self.ligand_list = filtered_ligand_list

        protein_list = []
        for entries in self.ligand_list:
            protein_list.append(entries.replace('ligand.sdf','protein.pdb'))
        self.protein_list = protein_list


        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor((len(self.ligand_list) * self.n_augmentations) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        batch_indicies = self.indexes[int(index*(self.batch_size/self.n_augmentations)):int((index+1)*(self.batch_size/self.n_augmentations))]
        # Generate data
        P, L = self.__data_generation(batch_indicies)

        return P, L

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.ligand_list)*self.n_augmentations)
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, batch_indicies):
        'Generates data containing batch_size samples' 
        # Initialization
        n_channels = len(self.atom_type_encoding)
        expanded_dim = self.dim + (n_channels,)
        #add channels here
        P = np.empty((self.batch_size, *expanded_dim))
        L = np.empty((self.batch_size, *expanded_dim))

        for index, each in enumerate(batch_indicies):
            #open/read sdf file and get center
            sdf_file = sdf_parser(self.ligand_list[each])
            sdf_coords = np.array(sdf_file)[:,0:3].astype(np.float)
            sdf_atoms = np.array(sdf_file)[:,3]
            center_coords = sdf_coords.mean(axis=0)
      
            #one-hot encode atoms
            encoded_atoms = np.empty((len(sdf_atoms),n_channels))
            for i, atom in enumerate(sdf_atoms):
                if atom not in self.atom_types:
                    atom = 'Other'
                encoded_atoms[i,:] = self.atom_type_encoding[self.atom_type_key[atom]]

            #augment coords -- REUSE this list of vectors to augment the ligand the same way
            rotation_vectors = np.random.randint(0, 359, (self.n_augmentations,3))
            augmented_sdf_coordinates, sdf_center_array = augment_predefined_angles(sdf_coords, rotation_vectors, center_coords)

            for i in range(self.n_augmentations):
                voxelized_sdf = voxelize(augmented_sdf_coordinates[i], encoded_atoms, center=sdf_center_array[i], voxel_size = self.voxel_size, grid_shape=self.dim)
                L[(index+i),:] = voxelized_sdf

            #now protein
            pdb_file = pdb_parser(self.protein_list[each])
            pdb_coords = np.array(pdb_file)[:,0:3].astype(np.float)
            pdb_atoms = np.array(pdb_file)[:,3]
            center_coords = pdb_coords.mean(axis=0)
      
            encoded_atoms = np.empty((len(pdb_atoms),n_channels))
            for i, atom in enumerate(pdb_atoms):
                if atom not in self.atom_types:
                    atom = 'Other'
                encoded_atoms[i,:] = self.atom_type_encoding[self.atom_type_key[atom]]
           
            #augment coords
            augmented_pdb_coordinates, center_array = augment_predefined_angles(pdb_coords, rotation_vectors, center_coords)

            for i in range(self.n_augmentations):
                voxelized_pdb = voxelize(augmented_pdb_coordinates[i], encoded_atoms, center=sdf_center_array[i], voxel_size = self.voxel_size, grid_shape=self.dim)
                P[(index+i),:] = voxelized_pdb

        return P, L
