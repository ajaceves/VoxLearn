from tensorflow import keras
from tensorflow.keras.layers import Input, Add, Conv3D, ReLU, BatchNormalization
"""
https://arxiv.org/pdf/1512.03385.pdf
https://towardsdatascience.com/building-a-resnet-in-keras-e8f1322a49ba
"""

def relu_bn(input, momentum):
    """
    a reLU activation followed by BatchNormalization
    params
    :input: (keras tensor) input tensor
    
    Returns: keras tensor
    """
    relu = ReLU()(input)
    bn = BatchNormalization(momentum=momentum)(relu)
    return bn

def residual_block(input, downsample, filters, momentum, kernel_size=3):
    """
    Residual block for resnet
    
    params
    :input: (keras tensor) input tensor 
    :downsample: (bool) increases stride to 2 if True
    :filters: (int) number of filters to apply in convolution
    :kernel_size: (int or 3 element tuple) convolution window
    
    Returns: keras tensor
    """
    
    N = Conv3D(kernel_size=kernel_size, strides= (1 if not downsample else 2), filters=filters, padding='same')(input)
    N = relu_bn(N, momentum)
    N = Conv3D(kernel_size=kernel_size, strides=1, filters=filters, padding='same')(N)
    
    if downsample:
        N_1 = Conv3D(kernel_size=1, strides=2, filters=filters, padding='same')(input)
        out = Add()([N_1, N])
        out = relu_bn(out, momentum)
        return out
    
    out = Add()([input, N])
    out = relu_bn(out, momentum)
    return out 
